@extends('layouts.master')

@section('title') {{ __('Purchase Request Management') }} @endsection

@section('css')
    <link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/libs/datepicker/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') <a href="{{ route('purchase-request.index') }}">{{ __('Purchase Request Management') }}</a> @endslot
        @slot('title') {{ __('Create Quotation') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Create New Quotation') }}</h4>

                    <form method="post" action="{{ route('purchase-request.store-quotation', $purchase_request) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Company') }}</label>
                                    <p>{{ $purchase_request->client->full_name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Purchase Code') }}</label>
                                    <p>{{ $purchase_request->purchase_code }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Delivery Location') }}</label>
                                    <p>{{ $purchase_request->location?->name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Expected Date') }}</label>
                                    <p>{{ $purchase_request->expected_date }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Purchase Request Remark') }}</label>
                                    <p>{{ $purchase_request->remark }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="inputValidUntilDate" class="form-label">{{ __('Valid Until Date') }}</label>
                                    <input type="text" name="valid_until" class="form-control" id="inputValidUntilDate" placeholder="{{ __('Enter') }} {{ __('Valid Until Date') }}" required="required" data-toggle="datepicker" autocomplete="off" value="{{ old('valid_until') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="inputRemark" class="form-label">{{ __('Quotation Remark') }}</label>
                                    <input type="text" name="remark" class="form-control" id="inputRemark" placeholder="{{ __('Enter') }} {{ __('Quotation Remark') }}" value="{{ old('remark') }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0" id="dynamicTable">

                                        <thead>
                                            <tr>
                                                <th>{{ __('No') }}</th>
                                                <th>{{ __('Product Name') }}</th>
                                                <th>{{ __('Description') }}</th>
                                                <th width="10%">{{ __('Quantity') }}</th>
                                                <th width="10%">{{ __('Quoted Quantity') }}</th>
                                                <th width="10%">{{ __('UoM') }}</th>
                                                <th width="20%">{{ __('Quoted Price') }}</th>
                                                <th width="20%">{{ __('Total') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($purchase_request->items as $purchase_item)
                                            <tr class="">
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>
                                                    <input value="{{ $purchase_item->product?->id }}" name="product[]" type="hidden">
                                                    <input value="{{ $purchase_item->product?->name }}" type="text" class="form-control" readonly="readonly">
                                                </td>
                                                <td>
                                                    <input value="{{ $purchase_item->product?->description }}" type="text" class="form-control" readonly="readonly">
                                                </td>
                                                <td>
                                                    <input type="number" name="quantity[]" class="form-control" value="{{ $purchase_item->quantity }}" readonly="readonly">
                                                </td>
                                                <td>
                                                    <input type="number" data-parsley-type="digits" name="quoted_quantity[]" class="form-control" value="0" required="required">
                                                </td>
                                                <td>
                                                    <input value="{{ $purchase_item->uom?->id }}" name="uom[]" type="hidden">
                                                    <input value="{{ $purchase_item->uom?->name }}" type="text" class="form-control" readonly="readonly">
                                                </td>
                                                <td><input type="number" data-parsley-type="digits" name="quoted_price[]" class="form-control" value="0" required="required"></td>
                                                <td><input type="number" name="total_price[]" class="form-control" value="0" readonly="readonly"></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot class="">
                                            <tr class="">
                                                <th></th>
                                                <th class="p-4" colspan="6">{{ __('Total') }}</th>
                                                <th class="p-4 text-right" id="totalPrice">0</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-md-12">
                                <a href="{{ route('purchase-request.index') }}" role="button" class="btn btn-outline-secondary">{{ __('Cancel') }}</a>
                                <button type="submit" class="btn btn-primary">{{ __('Create Quotation') }}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
    <script>
    $(document).ready(function() {

        $("#dynamicTable").parsley();

        var priceElement = 'td:nth-child(7) input';
        var quantityElement = 'td:nth-child(5) input';

        applyTableStripe();

        $('#dynamicTable').on('click', '.removeRow', function() {
            var element = $(this).parents('tr');
            element.remove();
            recalculateRowNumber();
        });

        $('#dynamicTable').on('change keyup', priceElement, function() {
            calculateTotalPrice();
        });

        $('#dynamicTable').on('change keyup', quantityElement, function() {
            calculateTotalPrice();
        });

        $('#addRowBtn').on('click', function() {
            var row = $('#dynamicTable').find('tbody tr:last-child');
            var target = $('#dynamicTable').find('tbody');
            var cloned = row.clone();
            cloned.find('input').val('').prop('readonly', false);
            cloned.find('td:nth-child(4) input').val('0').prop('readonly', true);
            cloned.appendTo(target);
            recalculateRowNumber();
            // console.log(row);
        });

        function recalculateRowNumber() {
            applyTableStripe();
            $('#dynamicTable').find('tbody tr').each(function(i,e) {
                $(e).find('th').html(i + 1);
            });
        }

        function applyTableStripe() {
            var rowLength = $('#dynamicTable').find('tbody tr').length;
            if (rowLength == 1) {
                disableRemoveButton($('#dynamicTable').find('tbody tr'));
            } else {
                $('#dynamicTable').find('tbody tr').each(function(i,e) {
                    enableRemoveButton($(e));
                });
            }
        }

        function disableRemoveButton(row) {
            row.find('button.removeRow').prop('disabled', true);
        }

        function enableRemoveButton(row){
            row.find('button.removeRow').prop('disabled', false);
        }

        function calculateTotalPrice() {
            var totalPrice = 0;
            $('#dynamicTable').find('tbody tr').each(function(i,e) {
                // Count subtotal in row
                var subTotal = $(e).find(priceElement).val() * $(e).find(quantityElement).val();
                // console.log(subTotal);
                if ( ! isNaN(parseInt(subTotal))) {
                    $(e).find('input[name^="total_price"]').val(subTotal);
                    totalPrice = totalPrice + parseInt(subTotal);
                }
            });
            $('#totalPrice').html(totalPrice);
        }

        $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'dd/mm/yyyy',
            // template: datepickerTemplate,
            // itemTag: 'div',
            // defaultItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // defaultMonthItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // defaultYearItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // highlightedClass: 'bg-indigo-300 text-white',
            // pickedClass: 'bg-indigo-500 text-white',
            // mutedClass: 'text-gray-500',
            // container: "::after",
            // offset: -45,
            // offset: 85,
        });
    });
    </script>

@endsection

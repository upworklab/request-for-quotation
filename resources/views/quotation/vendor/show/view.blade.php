@extends('layouts.master')

@section('title') {{ __('Purchase Request') }} @endsection

@section('css')
    <link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/libs/datepicker/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') <a href="{{ route('quotation.index') }}">{{ __('Quotation') }}</a> @endslot
        @slot('title') {{ __('Quotation') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />
                    <x-notification />

                    <h4 class="card-title mb-4">{{ __('Quotation Detail') }}</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Company') }}</label>
                                    <p>{{ $quotation->client->full_name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Purchase Code') }}</label>
                                    <p>{{ $quotation->purchase_code }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Delivery Location') }}</label>
                                    <p>{{ $quotation->location->name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Expected Date') }}</label>
                                    <p>{{ $quotation->expected_date }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Remark') }}</label>
                                    <p>{{ $quotation->remark }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Valid Until Date') }}</label>
                                    <p>{{ $quotation->remote_valid_until }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0" id="dynamicTable">

                                        <thead>
                                            <tr>
                                                <th>{{ __('No') }}</th>
                                                <th>{{ __('Product Name') }}</th>
                                                <th>{{ __('Description') }}</th>
                                                <th width="10%">{{ __('Quantity') }}</th>
                                                <th width="10%">{{ __('UoM') }}</th>
                                                <th width="10%">{{ __('Quoted Price') }}</th>
                                                <th width="10%">{{ __('Total') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $grandTotal = 0;
                                            @endphp

                                            @foreach ($quotation->items as $purchase_item)
                                            @php
                                            $subTotal = $purchase_item->quoted_price * $purchase_item->quantity;
                                            $grandTotal = $grandTotal + $subTotal;
                                            @endphp
                                            <tr class="">
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $purchase_item->product?->name }}</td>
                                                <td>{{ $purchase_item->product?->description }}</td>
                                                <td>{{ $purchase_item->quantity }}</td>
                                                <td>{{ $purchase_item->uom?->name }}</td>
                                                <td>{{ $purchase_item->quoted_price }}</td>
                                                <td>{{ $subTotal }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot class="">
                                            <tr class="">
                                                <th></th>
                                                <th class="p-4" colspan="5">{{ __('Total') }}</th>
                                                <th class="text-right">{{ $grandTotal }}</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>


                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

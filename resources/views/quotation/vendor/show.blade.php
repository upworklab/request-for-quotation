@if ($quotation->status == "DRAFT")
    @include('quotation.vendor.show.edit')
@else
    @include('quotation.vendor.show.view')
@endif

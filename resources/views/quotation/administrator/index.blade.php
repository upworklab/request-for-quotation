@extends('layouts.master')

@section('title') Quotation @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Dashboard @endslot
        @slot('title') Quotation List @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <!-- <h4 class="card-title">User List</h4> -->
                    <!-- <p class="card-title-desc"></p> -->

                    <table id="dataTable" class="table table-bordered dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>{{ __('Request Date') }}</th>
                                <th>{{ __('Company') }}</th>
                                <th>{{ __('Purchase Code') }}</th>
                                <th>{{ __('Delivery Location') }}</th>
                                <th>{{ __('Expected Date') }}</th>
                                <th>{{ __('Vendor') }}</th>
                                <th>{{ __('Quotation Code') }}</th>
                                <th>{{ __('Valid Until') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/libs/pdfmake/pdfmake.min.js') }}"></script>

    <script src="{{ asset('js/datatables/plugin.js') }}"></script>
    <script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
    <script>
    $(document).ready(function() {

        var table = $('#dataTable').DataTable({
            autoWidth: false,
            // lengthChange: false,
            searching: true,
            processing: true,
            serverSide: true,
            responsive: true,
            order: [[0, "asc"]],
            ajax: {
                url: '{{ route("quotation.data") }}',
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
                statusCode: {
                    403: function() {
                        window.location.href = '{{ route("home") }}';
                    }
                }
            },
            columns: [
                {
                    data: 'created_at',
                    render: function ( data, type, row, meta ) {
                        return moment.utc(data).format('DD/MM/YYYY');
                    },
                },
                {
                    data: 'purchase_request.client',
                    name: 'purchase_request.client.full_name',
                    render: function ( data, type, row, meta ) {
                        return '<a href="{{ route('user.index') }}/'+data.user.id+'">'+data.full_name+'</a>';
                    },
                },
                {
                    data: 'purchase_request',
                    name: 'purchase_request.purchase_code',
                    render: function ( data, type, row, meta ) {
                        return '<a href="{{ route('purchase-request.index') }}/'+row.purchase_request.id+'">'+data.purchase_code+'</a>';
                    },
                },
                {
                    data: 'purchase_request',
                    name: 'purchase_request.delivery_location',
                    render: function ( data, type, row, meta ) {
                        return data.delivery_location;
                    },
                },
                {
                    data: 'purchase_request',
                    name: 'purchase_request.expected_date',
                    render: function ( data, type, row, meta ) {
                        return data.expected_date;
                    },
                },
                {
                    data: 'vendor',
                    name: 'vendor.full_name',
                    render: function ( data, type, row, meta ) {
                        return '<a href="{{ route('user.index') }}/'+row.vendor.user.id+'">'+row.vendor.full_name+'</a>';
                    },
                },
                {
                    data: 'quotation_code',
                    render: function ( data, type, row, meta ) {
                        return '<a href="{{ route('quotation.index') }}/'+row.id+'">'+data+'</a>';
                    },
                },
                {
                    data: 'valid_until',
                },
                { data: 'status' },
                {
                    sortable: false,
                    data: 'id',
                    render: function ( data, type, row, meta ) {
                        var buttonClass = "btn btn-primary btn-sm";
                        var targetUrl = "{{ route('quotation.index') }}/"+data;
                        var button = '<a href="'+targetUrl+'" class="'+buttonClass+'" data-uuid="'+data+'">View</a>';
                        return button;
                    },
                },
            ]
        });

        $(".dataTables_length select").addClass('form-select form-select-sm');

        $('#searchTable').keyup(delayResponse(function (e) {
            table.search( this.value ).draw();
        }, 500));

    });
    </script>

@endsection

@if ($quotation->status == "DRAFT")
    @include('quotation.administrator.show.edit')
@else
    @include('quotation.administrator.show.view')
@endif

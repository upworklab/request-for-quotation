@extends('layouts.master')

@section('title') {{ __('Purchase Request Management') }} @endsection

@section('css')
    <link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/libs/datepicker/datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
    .select2-container {
        position: relative;
        z-index: 2;
        float: left;
        width: 100%;
        margin-bottom: 0;
        display: table;
        table-layout: fixed;
    }

    </style>
    @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') <a href="{{ route('purchase-request.index') }}">{{ __('Purchase Request Management') }}</a> @endslot
        @slot('title') {{ __('Create Quotation') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Create New Quotation') }}</h4>

                    <form method="post" action="{{ route('purchase-request.store-quotation', $purchase_request) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Company') }}</label>
                                    <p>{{ $purchase_request->client->full_name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Purchase Code') }}</label>
                                    <p>{{ $purchase_request->purchase_code }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Delivery Location') }}</label>
                                    <p>{{ $purchase_request->location?->name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Expected Date') }}</label>
                                    <p>{{ $purchase_request->expected_date }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Purchase Request Remark') }}</label>
                                    <p>{{ $purchase_request->remark }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="selectVendor" class="form-label">{{ __('Vendor') }}</label>
                                    <select id="selectVendor" name="vendor" class="form-select" required="required">
                                        @foreach ($vendors as $vendor)
                                        <option value="{{ $vendor->remote_id }}">{{ $vendor->full_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="inputValidUntilDate" class="form-label">{{ __('Valid Until Date') }}</label>
                                    <input type="text" name="valid_until" class="form-control" id="inputValidUntilDate" placeholder="{{ __('Enter') }} {{ __('Valid Until Date') }}" required="required" data-toggle="datepicker" autocomplete="off" value="{{ old('valid_until') }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="inputRemark" class="form-label">{{ __('Quotation Remark') }}</label>
                                    <input type="text" name="remark" class="form-control" id="inputRemark" placeholder="{{ __('Enter') }} {{ __('Quotation Remark') }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0" id="dynamicTable">

                                        <thead>
                                            <tr>
                                                <th>{{ __('No') }}</th>
                                                <th>{{ __('Product Name') }}</th>
                                                <th width="10%">{{ __('Quantity') }}</th>
                                                <th width="10%">{{ __('Quoted Quantity') }}</th>
                                                <th width="10%">{{ __('UoM') }}</th>
                                                <th width="20%">{{ __('Quoted Price') }}</th>
                                                <th>{{ __('Action') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($purchase_request->items as $purchase_item)
                                            <tr class="">
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>
                                                    <select name="product[]" class="form-control product-select select2">
                                                        <option value="{{ $purchase_item->product?->remote_id }}">{{ $purchase_item->product?->name }}</option>
                                                    </select>
                                                </td>
                                                <td><input type="number" name="quantity[]" class="form-control" value="{{ $purchase_item->quantity }}" required="required" readonly="readonly"></td>
                                                <td><input type="number" name="quoted_quantity[]" class="form-control" value="" required="required"></td>
                                                <td>
                                                    <select name="uom[]" class="form-control uom-select select2">
                                                        <option value="{{ $purchase_item->uom?->remote_id }}">{{ $purchase_item->uom?->name }}</option>
                                                    </select>
                                                </td>
                                                <!-- <td><input type="text" name="uom[]" class="form-control" value="{{ $purchase_item->uom }}" required="required" readonly="readonly"></td> -->
                                                <td><input type="text" name="quoted_price[]" class="form-control" value="" required="required"></td>
                                                <td>
                                                    <button type="button" class="removeRow btn btn-danger">
                                                        <i class="bx bx-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot class="">
                                            <tr class="">
                                                <th></th>
                                                <th class="p-4" colspan="4">{{ __('Total') }}</th>
                                                <th class="p-4 text-right" id="totalPrice">0</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row my-2">
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="button" id="addRowBtn">
                                    {{ __('Add Row') }}
                                </button>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-md-12">
                                <a href="{{ route('purchase-request.index') }}" role="button" class="btn btn-outline-secondary">{{ __('Cancel') }}</a>
                                <button type="submit" class="btn btn-primary">{{ __('Create Quotation') }}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datepicker/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('js/ajaxsetup.js') }}"></script>
    <script>
    $(document).ready(function() {

        var priceElement = 'td:nth-child(6) input';

        applyTableStripe();
        initializeSelect2();

        $('#dynamicTable').on('click', '.removeRow', function() {
            var element = $(this).parents('tr');
            element.remove();
            recalculateRowNumber();
        });

        $('#dynamicTable').on('change keyup', priceElement, function() {
            // console.log($(this).val());
            calculateTotalPrice();
        });

        $('#addRowBtn').on('click', function() {
            var row = $('#dynamicTable').find('tbody tr:last-child');
            var target = $('#dynamicTable').find('tbody');
            destroySelect2();
            var cloned = row.clone();
            cloned.find('select').val('').html('');
            cloned.find('input').val('').prop('readonly', false);
            cloned.find('td:nth-child(3) input').val('0').prop('readonly', true);
            cloned.appendTo(target);
            recalculateRowNumber();
            initializeSelect2();
            // console.log(row);
        });

        function recalculateRowNumber() {
            applyTableStripe();
            $('#dynamicTable').find('tbody tr').each(function(i,e) {
                $(e).find('th').html(i + 1);
            });
        }

        function applyTableStripe() {
            var rowLength = $('#dynamicTable').find('tbody tr').length;
            if (rowLength == 1) {
                disableRemoveButton($('#dynamicTable').find('tbody tr'));
            } else {
                $('#dynamicTable').find('tbody tr').each(function(i,e) {
                    enableRemoveButton($(e));
                });
            }
        }

        function disableRemoveButton(row) {
            row.find('button.removeRow').prop('disabled', true);
        }

        function enableRemoveButton(row){
            row.find('button.removeRow').prop('disabled', false);
        }

        function calculateTotalPrice() {
            var totalPrice = 0;
            $('#dynamicTable').find('tbody tr').each(function(i,e) {
                var subTotal = $(e).find(priceElement).val();
                // console.log(subTotal);
                if ( ! isNaN(parseInt(subTotal))) {
                    totalPrice = totalPrice + parseInt(subTotal);
                }
            });
            $('#totalPrice').html(totalPrice);
        }

        $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'dd/mm/yyyy',
            // template: datepickerTemplate,
            // itemTag: 'div',
            // defaultItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // defaultMonthItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // defaultYearItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // highlightedClass: 'bg-indigo-300 text-white',
            // pickedClass: 'bg-indigo-500 text-white',
            // mutedClass: 'text-gray-500',
            // container: "::after",
            // offset: -45,
            // offset: 85,
        });

        // $(".product-select").each(function(i,e) {
        //     // console.log($(e).children('option'));
        //     // console.log($(e).val());
        //     // console.log($(e).text());
        //     var option = new Option($(e).text(), $(e).val(), true, true);
        //     $(e).append(option).trigger('change');
        // })

        function destroySelect2() {
            $(".product-select").each(function(i,e) {
                $(e).select2('destroy');
            });
            $(".uom-select").each(function(i,e) {
                $(e).select2('destroy');
            });
        }

        function initializeSelect2() {
            $(".product-select").each(function(i,e) {
                initializeProductSelect($(e));
            });
            $(".uom-select").each(function(i,e) {
                initializeUomSelect($(e));
            });
        }


        function initializeProductSelect(e) {
            e.select2({
                ajax: {
                    url: "{{ route('api.products.search') }}",
                    dataType: "json",
                    delay: 250,
                    data: function (e) {
                        return { q: e.term, page: e.page };
                    },
                    processResults: function (e, t) {
                        return (
                            (t.page = t.page || 1),
                            {
                                results: e.data,
                                pagination: { more: 10 * t.page < e.meta.total },
                            }
                        );
                    },
                    cache: !0,
                },
                placeholder: "Search ...",
                minimumInputLength: 1,
                templateResult: function (e) {
                    if (e.loading) return e.text;
                    var t = $(
                        "<div class='select2-result-repository clearfix'><div class='select2-result-repository__avatar'>" +
                        "</div><div class='select2-result-repository__meta'><div class='select2-result-repository__title'></div><div class='select2-result-repository__description'></div></div></div>"
                    );
                    return (
                        t.find(".select2-result-repository__title").text(e.itemName),
                        t.find(".select2-result-repository__description").text(e.itemDescription),
                        t
                    );
                },
                templateSelection: function (e) {
                    return e.itemName || e.text;
                },
            });
        }

        function initializeUomSelect(e) {
            e.select2({
                ajax: {
                    url: "{{ route('api.uoms.search') }}",
                    dataType: "json",
                    delay: 250,
                    data: function (e) {
                        return { q: e.term, page: e.page };
                    },
                    processResults: function (e, t) {
                        return (
                            (t.page = t.page || 1),
                            {
                                results: e.data,
                                pagination: { more: 10 * t.page < e.meta.total },
                            }
                        );
                    },
                    cache: !0,
                },
                placeholder: "Search ...",
                minimumInputLength: 1,
                templateResult: function (e) {
                    if (e.loading) return e.text;
                    var t = $(
                        "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'><div class='select2-result-repository__title'></div></div></div>"
                    );
                    return (
                        t.find(".select2-result-repository__title").text(e.itemName),
                        t
                    );
                },
                templateSelection: function (e) {
                    return e.itemName || e.text;
                },
            });
        }

        // $('#dynamicTable').on('select2:select', '.product-select', function(e) {
        $('#dynamicTable').on('select2:select', '.select2', function(e) {
            console.log(e);
            var selectedSelect = e.currentTarget;
            $(selectedSelect).html('').append(new Option(e.params.data.itemName, e.params.data.id, true, true))
        })

    });
    </script>

@endsection

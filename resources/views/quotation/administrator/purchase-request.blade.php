@extends('layouts.master')

@section('title') Purchase Request Management @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
    @slot('li_1') <a href="{{ route('purchase-request.index') }}">{{ __('Purchase Request Management') }}</a> @endslot
    @slot('title') {{ __('Quotations') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Purchase Request Detail') }}</h4>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Company') }}</label>
                                <p>{{ $purchase_request->client->full_name }}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Purchase Code') }}</label>
                                <p>{{ $purchase_request->purchase_code }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Delivery Location') }}</label>
                                <p>{{ $purchase_request->delivery_location }}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Expected Date') }}</label>
                                <p>{{ $purchase_request->expected_date }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Remark') }}</label>
                                <p>{{ $purchase_request->remark }}</p>
                            </div>
                        </div>
                    </div>


                    <table id="dataTable" class="table table-bordered dt-responsive  nowrap w-100">
                        <thead>
                            <tr>
                                <th>{{ __('Quotation Date') }}</th>
                                <th>{{ __('Vendor') }}</th>
                                <th>{{ __('Quotation Code') }}</th>
                                <th>{{ __('Valid Until') }}</th>
                                <th>{{ __('Total Price') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/libs/pdfmake/pdfmake.min.js') }}"></script>

    <script src="{{ asset('js/datatables/plugin.js') }}"></script>
    <script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
    <script>
    $(document).ready(function() {

        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'IDR',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
        });

        var table = $('#dataTable').DataTable({
            autoWidth: false,
            // lengthChange: false,
            searching: true,
            processing: true,
            serverSide: true,
            responsive: true,
            order: [[0, "asc"]],
            ajax: {
                url: '{{ route("purchase-request.datatable-quotation", $purchase_request) }}',
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
                statusCode: {
                    403: function() {
                        window.location.href = '{{ route("home") }}';
                    }
                }
            },
            columns: [
                {
                    data: 'created_at',
                    render: function ( data, type, row, meta ) {
                        return moment.utc(data).format('DD/MM/YYYY');
                    },
                },
                {
                    data: 'vendor',
                    name: 'vendor.full_name',
                    render: function ( data, type, row, meta ) {
                        return data.full_name;
                    },
                },
                { data: 'quotation_code' },
                {
                    data: 'valid_until',
                    // render: function ( data, type, row, meta ) {
                    //     return moment.utc(data).format('DD/MM/YYYY');
                    // },
                },
                {
                    data: 'total_value',
                    render: function ( data, type, row, meta ) {
                        return formatter.format(data);
                    },
                },
                {
                    sortable: false,
                    data: 'id',
                    render: function ( data, type, row, meta ) {
                        var buttonClass = "btn btn-primary btn-sm";
                        var targetUrl = "{{ route('quotation.index') }}/"+data;
                        var button = '<a href="'+targetUrl+'" class="'+buttonClass+'" data-uuid="'+data+'">View</a>';
                        return button;
                    },
                },
            ]
        });

        $(".dataTables_length select").addClass('form-select form-select-sm');

        $('#searchTable').keyup(delayResponse(function (e) {
            table.search( this.value ).draw();
        }, 500));

    });
    </script>

@endsection

@extends('layouts.master')

@section('title') {{ __('Purchase Request Management') }} @endsection

@section('css')
    <link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/libs/datepicker/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('actionButtons')
        <form method="post" action="{{ route('quotation.finalize', $quotation) }}">
            @method('PATCH')
            @csrf
            <button class="btn btn-primary" type="submit">
                <i class="bx bx-mail-send me-1"></i> {{ __('Finalize') }}
            </button>
        </form>
        @endslot
        @slot('li_1') <a href="{{ route('purchase-request.index') }}">{{ __('Purchase Request Management') }}</a> @endslot
        @slot('title') {{ __('Edit Quotation') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Purchase Request Detail') }}</h4>

                    <form method="post" action="{{ route('quotation.update', $quotation) }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Company') }}</label>
                                    <p>{{ $purchase_request->client->full_name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Purchase Code') }}</label>
                                    <p>{{ $purchase_request->purchase_code }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Delivery Location') }}</label>
                                    <p>{{ $purchase_request->delivery_location }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Expected Date') }}</label>
                                    <p>{{ $purchase_request->expected_date }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Remark') }}</label>
                                    <p>{{ $purchase_request->remark }}</p>
                                </div>
                            </div>
                        </div>

                        <h4 class="card-title my-4">{{ __('Quotation Detail') }}</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="selectVendor" class="form-label">{{ __('Vendor') }}</label>
                                    <select id="selectVendor" name="vendor" class="form-select" required="required">
                                        @foreach ($vendors as $vendor)
                                        <option value="{{ $vendor->id }}">{{ $vendor->full_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="inputValidUntilDate" class="form-label">{{ __('Valid Until Date') }}</label>
                                    <input type="text" name="valid_until" class="form-control" id="inputValidUntilDate" placeholder="{{ __('Enter') }} {{ __('Valid Until Date') }}" required="required" data-toggle="datepicker" autocomplete="off" value="{{ old('valid_until', $quotation->valid_until) }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="inputRemark" class="form-label">{{ __('Remark') }}</label>
                                    <input type="text" name="remark" class="form-control" id="inputRemark" placeholder="{{ __('Enter') }} {{ __('Quotation Remark') }}" value="{{ old('remark', $quotation->remark) }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0" id="dynamicTable">

                                        <thead>
                                            <tr>
                                                <th>{{ __('No') }}</th>
                                                <th>{{ __('Product Name') }}</th>
                                                <th>{{ __('Description') }}</th>
                                                <th width="10%">{{ __('Quantity') }}</th>
                                                <th width="10%">{{ __('Quoted Quantity') }}</th>
                                                <th width="10%">{{ __('UoM') }}</th>
                                                <th width="20%">{{ __('Quoted Price') }}</th>
                                                <th>{{ __('Action') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($quotation->items as $quotation_item)
                                            <tr class="">
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td><input type="text" name="product_name[]" class="form-control" value="{{ $quotation_item->product_name }}" required="required" readonly="readonly"></td>
                                                <td><input type="text" name="product_description[]" class="form-control" value="{{ $quotation_item->product_description }}" readonly="readonly"></td>
                                                <td><input type="number" name="quantity[]" class="form-control" value="{{ $quotation_item->quantity_requested }}" required="required" readonly="readonly"></td>
                                                <td><input type="number" name="quoted_quantity[]" class="form-control" value="{{ $quotation_item->quantity_quoted }}" required="required"></td>
                                                <td><input type="text" name="uom[]" class="form-control" value="{{ $quotation_item->uom }}" required="required" readonly="readonly"></td>
                                                <td class="input-group">
                                                    <span class="input-group-text">{{ __('IDR') }}</span>
                                                    <input type="text" name="quoted_price[]" class="form-control" value="{{ $quotation_item->price_quoted }}" required="required">
                                                </td>
                                                <td>
                                                    <button type="button" class="removeRow btn btn-danger">
                                                        <i class="bx bx-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot class="">
                                            <tr class="">
                                                <th></th>
                                                <th class="p-4" colspan="5">{{ __('Total') }}</th>
                                                <th class="p-4" id="totalPrice">{{ $quotation->total_value }}</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row my-2">
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="button" id="addRowBtn">
                                    {{ __('Add Row') }}
                                </button>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-md-12">
                                <a href="{{ route('purchase-request.index') }}" role="button" class="btn btn-outline-secondary">Cancel</a>
                                <button type="submit" class="btn btn-primary">Save Quotation</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datepicker/datepicker.min.js') }}"></script>
    <script>
    $(document).ready(function() {

        var priceElement = 'td:nth-child(7) input';

        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: '{{ __('IDR') }}',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
        });

        applyTableStripe();

        $('#dynamicTable').on('click', '.removeRow', function() {
            var element = $(this).parents('tr');
            element.remove();
            recalculateRowNumber();
        });

        $('#dynamicTable').on('change keyup', priceElement, function() {
            // console.log($(this).val());
            calculateTotalPrice();
        });

        $('#addRowBtn').on('click', function() {
            var row = $('#dynamicTable').find('tbody tr:last-child');
            var target = $('#dynamicTable').find('tbody');
            var cloned = row.clone();
            cloned.find('input').val('').prop('readonly', false);
            cloned.find('td:nth-child(4) input').val('0').prop('readonly', true);
            cloned.appendTo(target);
            recalculateRowNumber();
            // console.log(row);
        });

        function recalculateRowNumber() {
            applyTableStripe();
            $('#dynamicTable').find('tbody tr').each(function(i,e) {
                $(e).find('th').html(i + 1);
            });
        }

        function applyTableStripe() {
            var rowLength = $('#dynamicTable').find('tbody tr').length;
            if (rowLength == 1) {
                disableRemoveButton($('#dynamicTable').find('tbody tr'));
            } else {
                $('#dynamicTable').find('tbody tr').each(function(i,e) {
                    enableRemoveButton($(e));
                });
            }
            calculateTotalPrice();
        }

        function disableRemoveButton(row) {
            row.find('button.removeRow').prop('disabled', true);
        }

        function enableRemoveButton(row){
            row.find('button.removeRow').prop('disabled', false);
        }

        function calculateTotalPrice() {
            var totalPrice = 0;
            $('#dynamicTable').find('tbody tr').each(function(i,e) {
                var subTotal = $(e).find(priceElement).val();
                // console.log(subTotal);
                if ( ! isNaN(parseInt(subTotal))) {
                    totalPrice = totalPrice + parseInt(subTotal);
                }
            });
            $('#totalPrice').html(formatter.format(totalPrice));
        }

        $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'dd/mm/yyyy',
        });
    });
    </script>

@endsection

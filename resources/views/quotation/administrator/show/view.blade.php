@extends('layouts.master')

@section('title') {{ __('Purchase Request Management') }} @endsection

@section('css')
    <link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/libs/datepicker/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') <a href="{{ route('purchase-request.index') }}">{{ __('Purchase Request Management') }}</a> @endslot
        @slot('title') {{ __('Quotation') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Purchase Request Detail') }}</h4>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Company') }}</label>
                                <p>{{ $purchase_request->client->full_name }}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Purchase Code') }}</label>
                                <p>{{ $purchase_request->purchase_code }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Delivery Location') }}</label>
                                <p>{{ $purchase_request->delivery_location }}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Expected Date') }}</label>
                                <p>{{ $purchase_request->expected_date }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Remark') }}</label>
                                <p>{{ $purchase_request->remark }}</p>
                            </div>
                        </div>
                    </div>

                    <h4 class="card-title my-4">{{ __('Quotation Detail') }}</h4>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Vendor') }}</label>
                                <p>{{ $quotation->vendor->full_name }}</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">{{ __('Valid Until Date') }}</label>
                                <p>{{ $quotation->valid_until }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="inputRemark" class="form-label">{{ __('Remark') }}</label>
                                <p>{{ $quotation->remark }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped mb-0" id="dynamicTable">

                                    <thead>
                                        <tr>
                                            <th>{{ __('No') }}</th>
                                            <th>{{ __('Product Name') }}</th>
                                            <th>{{ __('Description') }}</th>
                                            <th width="10%">{{ __('Quantity') }}</th>
                                            <th width="10%">{{ __('Quoted Quantity') }}</th>
                                            <th width="10%">{{ __('UoM') }}</th>
                                            <th width="20%">{{ __('Quoted Price') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($quotation->items as $quotation_item)
                                        <tr class="">
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>
                                                <p>{{ $quotation_item->product_name }}</p>
                                            </td>
                                            <td>
                                                <p>{{ $quotation_item->product_description }}</p>
                                            </td>
                                            <td>
                                                <p>{{ $quotation_item->quantity_requested }}</p>
                                            </td>
                                            <td>
                                                <p>{{ $quotation_item->quantity_quoted }}</p>
                                            </td>
                                            <td>
                                                <p>{{ $quotation_item->uom }}</p>
                                            </td>
                                            <td class="formatCurrency">{{ $quotation_item->price_quoted }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot class="">
                                        <tr class="">
                                            <th></th>
                                            <th class="p-4" colspan="5">{{ __('Total') }}</th>
                                            <th class="formatCurrency">{{ $quotation->total_value }}</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <script>
    $(document).ready(function() {

        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: '{{ __('IDR') }}',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
        });

        $('.formatCurrency').each(function(e,v) {
            var that = $(v);
            var value = that.text();
            that.text(formatter.format(value));
        });

    });
    </script>

@endsection

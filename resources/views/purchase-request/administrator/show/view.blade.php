@extends('layouts.master')

@section('title') {{ __('Purchase Request Management') }} @endsection

@section('css')
    <link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/libs/datepicker/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') <a href="{{ route('purchase-request.index') }}">{{ __('Purchase Request Management') }}</a> @endslot
        @slot('title') {{ __('Purchase Request') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Purchase Request Detail') }}</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Company') }}</label>
                                    <p>{{ $purchase_request->client->full_name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Purchase Code') }}</label>
                                    <p>{{ $purchase_request->purchase_code }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Delivery Location') }}</label>
                                    <p>{{ $purchase_request->location->name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Expected Date') }}</label>
                                    <p>{{ $purchase_request->expected_date }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">{{ __('Remark') }}</label>
                                    <p>{{ $purchase_request->remark }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0" id="dynamicTable">

                                        <thead>
                                            <tr>
                                                <th>{{ __('No') }}</th>
                                                <th>{{ __('Product Name') }}</th>
                                                <!-- <th>{{ __('Description') }}</th> -->
                                                <th width="10%">{{ __('Quantity') }}</th>
                                                <th width="10%">{{ __('UoM') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($purchase_request->items as $purchase_item)
                                            <tr class="">
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $purchase_item->product?->name }}</td>
                                                <!-- <td>{{ $purchase_item->product_description }}</td> -->
                                                <td>{{ $purchase_item->quantity }}</td>
                                                <td>{{ $purchase_item->uom?->name }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@extends('layouts.master')

@section('title') {{ __('Purchase Request Management') }} @endsection

@section('css')
    <link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/libs/datepicker/datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') <a href="{{ route('purchase-request.index') }}">{{ __('Purchase Request Management') }}</a> @endslot
        @slot('title') {{ __('Edit Purchase Request') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Purchase Request Detail') }}</h4>

                    <form method="post" action="{{ route('purchase-request.update', $purchase_request) }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="selectCompany" class="form-label">{{ __('Company') }}</label>
                                    <select id="selectCompany" name="company" class="form-select">
                                        @foreach ($clients as $client)
                                        <option value="{{ $client->id }}">{{ $client->full_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="inputPurchaseCode" class="form-label">{{ __('Purchase Code') }}</label>
                                    <input type="text" name="purchase_code" class="form-control" id="inputPurchaseCode" placeholder="{{ __('Enter') }} {{ __('Purchase Code') }}" value="{{ old('purchase_code', $purchase_request->purchase_code) }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="inputDeliveryLocation" class="form-label">{{ __('Delivery Location') }}</label>
                                    <input type="text" name="delivery_location" class="form-control" id="inputDeliveryLocation" placeholder="{{ __('Enter') }} {{ __('Delivery Location') }}" value="{{ old('delivery_location', $purchase_request->delivery_location) }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="inputExpectedDate" class="form-label">{{ __('Expected Date') }}</label>
                                    <input type="text" name="expected_date" class="form-control" id="inputExpectedDate" placeholder="{{ __('Enter') }} {{ __('Expected Date') }}" data-toggle="datepicker" autocomplete="off" value="{{ old('expected_date', $purchase_request->expected_date) }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="inputRemark" class="form-label">{{ __('Remark') }}</label>
                                    <input type="text" name="remark" class="form-control" id="inputRemark" placeholder="{{ __('Enter') }} {{ __('Remark') }}" value="{{ old('remark', $purchase_request->remark) }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0" id="dynamicTable">

                                        <thead>
                                            <tr>
                                                <th>{{ __('No') }}</th>
                                                <th>{{ __('Product Name') }}</th>
                                                <th width="10%">{{ __('Quantity') }}</th>
                                                <th width="10%">{{ __('UoM') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($purchase_request->items->count() > 0)
                                            @foreach ($purchase_request->items as $purchase_item)
                                            <tr class="">
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>
                                                    <select name="product[]" class="form-control product-select select2">
                                                        <option value="{{ $purchase_item->product?->remote_id }}">{{ $purchase_item->product?->name }}</option>
                                                    </select>
                                                </td>
                                                <td><input type="number" name="quantity[]" class="form-control" value="{{ $purchase_item->quantity }}" required="required"></td>
                                                <td>
                                                    <select name="uom[]" class="form-control uom-select select2">
                                                        <option value="{{ $purchase_item->uom?->remote_id }}">{{ $purchase_item->uom?->name }}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr class="">
                                                <th scope="row">1</th>
                                                <td><input type="text" name="product_name[]" class="form-control" value="" required="required"></td>
                                                <td><input type="number" name="quantity[]" class="form-control" value="" required="required"></td>
                                                <td><input type="text" name="uom[]" class="form-control" value="" required="required"></td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-md-12">
                                <a href="{{ route('purchase-request.index') }}" role="button" class="btn btn-outline-secondary">{{ __('Cancel') }}</a>
                                <button type="submit" class="btn btn-primary">{{ __('Update Purchase Request') }}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datepicker/datepicker.min.js') }}"></script>
    <script>
    $(document).ready(function() {

        applyTableStripe();

        $('#dynamicTable').on('click', '.removeRow', function() {
            var element = $(this).parents('tr');
            element.remove();
            recalculateRowNumber();
        });

        $('#addRowBtn').on('click', function() {
            var row = $('#dynamicTable').find('tbody tr:last-child');
            var target = $('#dynamicTable').find('tbody');
            var cloned = row.clone();
            // var inputBaseClass = 'bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500';
            cloned.find('input').val('');
            cloned.appendTo(target);
            recalculateRowNumber();
        });

        function recalculateRowNumber() {
            applyTableStripe();
            $('#dynamicTable').find('tbody tr').each(function(i,e) {
                $(e).find('th').html(i + 1);
            });
        }

        function applyTableStripe() {
            var rowLength = $('#dynamicTable').find('tbody tr').length;
            if (rowLength == 1) {
                disableRemoveButton($('#dynamicTable').find('tbody tr'));
            } else {
                $('#dynamicTable').find('tbody tr').each(function(i,e) {
                    enableRemoveButton($(e));
                    // if (i % 2 != 0) {
                    //     $(e).removeClass().addClass('border-b dark:border-gray-700 bg-gray-50 dark:bg-gray-700');
                    // } else {
                    //     $(e).removeClass().addClass('border-b dark:border-gray-700 bg-white dark:bg-gray-800');
                    // }
                });
            }
        }

        function disableRemoveButton(row) {
            row.find('button.removeRow').prop('disabled', true);
        }

        function enableRemoveButton(row){
            row.find('button.removeRow').prop('disabled', false);
        }

        $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'dd/mm/yyyy',
            // template: datepickerTemplate,
            // itemTag: 'div',
            // defaultItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // defaultMonthItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // defaultYearItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // highlightedClass: 'bg-indigo-300 text-white',
            // pickedClass: 'bg-indigo-500 text-white',
            // mutedClass: 'text-gray-500',
            // container: "::after",
            // offset: -45,
            // offset: 85,
        });
    });
    </script>

@endsection

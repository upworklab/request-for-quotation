@extends('layouts.master')

@section('title') Purchase Request Management @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Dashboard @endslot
        @slot('title') Purchase Request Management @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <!-- <h4 class="card-title">User List</h4> -->
                    <!-- <p class="card-title-desc"></p> -->

                    <table id="dataTable" class="table table-bordered dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>{{ __('Request Date') }}</th>
                                <th>{{ __('Purchase Code') }}</th>
                                <th>{{ __('Location') }}</th>
                                <th>{{ __('Company') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/libs/pdfmake/pdfmake.min.js') }}"></script>

    <script src="{{ asset('js/datatables/plugin.js') }}"></script>
    <script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
    <script>
    $(document).ready(function() {

        var table = $('#dataTable').DataTable({
            autoWidth: false,
            // lengthChange: false,
            searching: true,
            processing: true,
            serverSide: true,
            responsive: true,
            order: [[0, "asc"]],
            ajax: {
                url: '{{ route("purchase-request.data") }}',
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
                statusCode: {
                    403: function() {
                        window.location.href = '{{ route("home") }}';
                    }
                }
            },
            columns: [
                {
                    data: 'created_at',
                    render: function ( data, type, row, meta ) {
                        return moment.utc(data).format('DD/MM/YYYY');
                    },
                },
                { data: 'purchase_code' },
                {
                    data: 'location',
                    name: 'location.name',
                    render: function ( data, type, row, meta ) {
                        return data.name;
                    },
                },
                {
                    data: 'client',
                    name: 'client.full_name',
                    render: function ( data, type, row, meta ) {
                        return data.full_name;
                    },
                },
                { data: 'status' },
                {
                    sortable: false,
                    data: 'id',
                    render: function ( data, type, row, meta ) {
                        var buttonClass = "btn btn-primary btn-sm";
                        var targetUrl = "{{ route('purchase-request.index') }}/"+data;
                        var button = '<a href="'+targetUrl+'" class="'+buttonClass+'" data-uuid="'+data+'">View</a>';
                        return button;
                    },
                },
            ]
        });

        $(".dataTables_length select").addClass('form-select form-select-sm');

        $('#searchTable').keyup(delayResponse(function (e) {
            table.search( this.value ).draw();
        }, 500));

    });
    </script>

@endsection

<x-app-layout>
    <x-slot name="header">
        <div class="flex items-center gap-4">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Create Purchase Request') }}
            </h2>
            <x-button-link href="{{ route('purchase-request.index') }}" class="">
                {{ __('Back') }}
            </x-button-link>
        </div>
    </x-slot>

    <div class="flex max-w-7xl px-8 mx-auto">
        <div class="flex-1 bg-white rounded-2xl mt-9 drop-shadow-lg overflow-x-auto">

            <form method="post" action="{{ route('purchase-request.store') }}" enctype="multipart/form-data">
                @csrf

                <div class="mx-8">

                    <x-validation-errors class="my-4" :errors="$errors" />

                    <div class="grid md:grid-cols-2 md:gap-6 my-4">
                        <div class="flex items-center">
                            <div class="w-1/4">
                                <x-label class="mb-0" for="purchaseCode" :value="__('Purchase Code')" />
                            </div>
                            <div class="w-3/4">
                                <x-input type="text" name="purchase_code" id="purchaseCode" value="{{ old('purchase_code') }}" required="required" />
                            </div>
                        </div>

                        <div class="flex items-center">
                            <div class="w-1/4">
                                <x-label class="mb-0" for="deliveryLocation" :value="__('Delivery Location')" />
                            </div>
                            <div class="w-3/4">
                                <x-input type="text" name="delivery_location" id="deliveryLocation" value="{{ old('delivery_location') }}" required="required" />
                            </div>
                        </div>

                        <div class="flex items-center">
                            <div class="w-1/4">
                                <x-label class="mb-0" for="expectedDate" :value="__('Expected Date')" />
                            </div>
                            <div class="w-3/4">
                                <x-input type="text" name="expected_date" id="expectedDate" value="{{ old('expected_date') }}" data-toggle="datepicker" autocomplete="off" required="required" />
                            </div>
                        </div>

                        <div class="flex items-center">
                            <div class="w-1/4">
                                <x-label class="mb-0" for="remark" :value="__('Remark')" />
                            </div>
                            <div class="w-3/4">
                                <x-input type="text" name="remark" id="remark" value="{{ old('remark') }}" />
                            </div>
                        </div>
                    </div>
                </div>

                <table class="w-full dataTable table-auto stripe" id="dataTable">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr class="text-left border-b border-white">
                            <th class="p-4">{{ __('No') }}</th>
                            <th class="p-4">{{ __('Product Name') }}</th>
                            <!-- <th class="p-4">{{ __('Description') }}</th> -->
                            <th class="p-4">{{ __('Quantity') }}</th>
                            <th class="p-4">{{ __('UoM') }}</th>
                            <th class="p-4">{{ __('Delete') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="">
                            <td>1</td>
                            <td>
                                <x-input type="text" name="product_name[]" class="w-48" value="" required="required" />
                            </td>
                            <!-- <td>
                                <x-input type="text" name="product_description[]" class="" value="" />
                            </td> -->
                            <td>
                                <x-input type="number" name="quantity[]" class="w-24" value="" required="required" />
                            </td>
                            <td>
                                <x-input type="text" name="uom[]" class="w-24" value="" required="required" />
                            </td>
                            <td>
                                <x-button type="button" class="removeRow">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                    </svg>
                                </x-button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="border-b border-gray-300">
                    <div class="mx-8 pt-6 mb-6 ">
                        <x-button class="mr-2" type="button" id="addRowBtn">
                            {{ __('Add Row') }}
                        </x-button>
                    </div>
                </div>


                <div class="mx-8 pt-8 mb-6">
                    <x-button-link href="{{ route('purchase-request.index') }}" class="mr-2">
                        {{ __('Cancel') }}
                    </x-button-link>
                    <x-button>
                        {{ __('Submit') }}
                    </x-button>
                </div>

            </form>

        </div>
    </div>

    @push('styles')
    <link href="{{ asset('css/datatables.custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
    @endpush

    @push('scripts')
    <script src="{{ asset('js/jquery/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/datepicker/datepicker-template.js') }}"></script>
    <script>
    $(document).ready(function() {
        applyTableStripe();


        $('#dataTable').on('click', '.removeRow', function() {
            var element = $(this).parents('tr');
            element.remove();
            recalculateRowNumber();
        });

        $('#addRowBtn').on('click', function() {
            var row = $('#dataTable').find('tbody tr:last-child');
            var target = $('#dataTable').find('tbody');
            var cloned = row.clone();
            // var inputBaseClass = 'bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500';
            cloned.find('input').val('');
            cloned.appendTo(target);
            recalculateRowNumber();
        });

        function recalculateRowNumber() {
            applyTableStripe();
            $('#dataTable').find('tbody tr').each(function(i,e) {
                $(e).find('td:nth-child(1)').html(i + 1);
            });
        }

        function applyTableStripe() {
            var rowLength = $('#dataTable').find('tbody tr').length;
            if (rowLength == 1) {
                disableRemoveButton($('#dataTable').find('tbody tr'));
            } else {
                $('#dataTable').find('tbody tr').each(function(i,e) {
                    enableRemoveButton($(e));
                    if (i % 2 != 0) {
                        $(e).removeClass().addClass('border-b dark:border-gray-700 bg-gray-50 dark:bg-gray-700');
                    } else {
                        $(e).removeClass().addClass('border-b dark:border-gray-700 bg-white dark:bg-gray-800');
                    }
                });
            }
        }

        function disableRemoveButton(row) {
            row.find('button.removeRow').prop('disabled', true);
        }

        function enableRemoveButton(row){
            row.find('button.removeRow').prop('disabled', false);
        }

        $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'dd/mm/yyyy',
            // template: datepickerTemplate,
            // itemTag: 'div',
            // defaultItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // defaultMonthItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // defaultYearItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
            // highlightedClass: 'bg-indigo-300 text-white',
            // pickedClass: 'bg-indigo-500 text-white',
            // mutedClass: 'text-gray-500',
            // // container: "::after",
            // offset: -45,
            // // offset: 85,
        });
    });
    </script>
    @endpush
</x-app-layout>

@extends('layouts.master')

@section('title') {{ __('User Management') }} @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') <a href="{{ route('user.index') }}">{{ __('User Management') }}</a> @endslot
        @slot('title') {{ __('Edit User') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('User Detail') }}</h4>

                    <form method="post" action="{{ route('user.update', $user) }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf

                        <div class="row mb-4">
                            <label for="userName" class="col-form-label col-lg-2">{{ __('Name') }}</label>
                            <div class="col-lg-10">
                                <input id="userName" name="name" type="text" class="form-control" placeholder="Enter User Name" value="{{ old('name', $user->name) }}">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="userEmail" class="col-form-label col-lg-2">{{ __('Email') }}</label>
                            <div class="col-lg-10">
                                <input id="userEmail" name="email" type="email" class="form-control" placeholder="Enter User Email" value="{{ old('email', $user->email) }}">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="userPassword" class="col-form-label col-lg-2">{{ __('Password') }}</label>
                            <div class="col-lg-10">
                                <input id="userPassword" name="password" type="password" class="form-control" placeholder="Enter User Password">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="userPasswordConfirmation" class="col-form-label col-lg-2">{{ __('Password Confirmation') }}</label>
                            <div class="col-lg-10">
                                <input id="userPasswordConfirmation" name="password_confirmation" type="password" class="form-control" placeholder="Confirm User Password">
                            </div>
                        </div>

                        <!-- Avatar -->
                        <div class="row mb-4">
                            <label for="avatar" class="col-form-label col-lg-2">{{ __('Avatar') }}</label>
                            <div class="col-lg-10">
                                <a href="{{ $user->avatar_path }}" target="_blank"><img src="{{ $user->avatar_path }}" class="dashboard-avatar my-2"></a>
                                <input id="avatar" name="avatar" type="file" class="form-control" >
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="userRole" class="col-form-label col-lg-2">{{ __('User Role') }}</label>
                            <div class="col-lg-10">
                                <select id="userRole" name="role" class="form-select">
                                    @foreach (\App\Models\Role::all() as $role)
                                    <option value="{{ $role->id }}" @selected($role->id == $user->roles()->first()->id)>{{ ucfirst($role->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!-- Related attributes -->
                        <div id="dynamicProperties">
                            @include('user.administrator.attributes.update.administrator')
                            @include('user.administrator.attributes.update.client')
                            @include('user.administrator.attributes.update.vendor')
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-lg-10">
                                <a href="{{ route('user.index') }}" role="button" class="btn btn-outline-secondary">{{ __('Cancel') }}</a>
                                <button type="submit" class="btn btn-primary">{{ __('Update User') }}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <script>
    $(document).ready(function() {

        $('select#userRole').on('change', function(i) {
            var selectedRole = $(this).find(':selected').text().toLowerCase();
            var targetProperty = selectedRole + 'Properties';
            $('#dynamicProperties').children('div').each(function(i) {
                $(this).attr('hidden', true).find('input').prop('disabled', true);
            })
            $('#dynamicProperties').find('#'+targetProperty).attr('hidden', false).find('input').prop('disabled', false);
        });

        $('select#userRole').trigger('change');

        // $('[data-toggle="datepicker"]').datepicker({
        //     autoHide: true,
        //     format: 'dd/mm/yyyy',
        //     template: datepickerTemplate,
        //     itemTag: 'div',
        //     defaultItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
        //     defaultMonthItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
        //     defaultYearItemClass: 'cursor-pointer text-center leading-none rounded-full leading-loose transition ease-in-out duration-100 hover:bg-gray-200',
        //     highlightedClass: 'bg-light-sea-green text-white',
        //     pickedClass: 'bg-plump-purple text-white',
        //     mutedClass: 'text-gray-500',
        //     // container: "::after",
        //     offset: -45,
        //     // offset: 85,
        // });


    });
    </script>

@endsection

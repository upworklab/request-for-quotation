<div id="clientProperties" hidden>
    <!-- Full Name -->
    <div class="row mb-4">
        <label for="clientFullName" class="col-form-label col-lg-2">{{ __('Full Name') }}</label>
        <div class="col-lg-10">
            <input id="clientFullName" name="full_name" type="text" class="form-control" placeholder="Client Full Name..." value="{{ old('full_name', $user->client?->full_name) }}">
        </div>
    </div>

    <!-- Alias -->
    <div class="row mb-4">
        <label for="clientAlias" class="col-form-label col-lg-2">{{ __('Alias') }}</label>
        <div class="col-lg-10">
            <input id="clientAlias" name="alias" type="text" class="form-control" placeholder="Client Alias..." value="{{ old('alias', $user->client?->alias) }}" >
        </div>
    </div>

    <!-- Domain -->
    <div class="row mb-4">
        <label for="clientDomain" class="col-form-label col-lg-2">{{ __('Address') }}</label>
        <div class="col-lg-10">
            <input id="clientDomain" name="domain" type="text" class="form-control" placeholder="Client Address..." value="{{ old('domain', $user->client?->domain) }}" >
        </div>
    </div>

</div>

<div id="vendorProperties" hidden>
    <!-- Vendor ID -->
    <div class="row mb-4">
        <label for="vendorRemoteId" class="col-form-label col-lg-2">{{ __('Vendor ID') }}</label>
        <div class="col-lg-10">
            <input id="vendorRemoteId" name="vendor_id" type="number" class="form-control" placeholder="Vendor ID..." value="{{ old('vendor_id', $user->vendor?->remote_id) }}" >
        </div>
    </div>

    <!-- Full Name -->
    <div class="row mb-4">
        <label for="vendorFullName" class="col-form-label col-lg-2">{{ __('Full Name') }}</label>
        <div class="col-lg-10">
            <input id="vendorFullName" name="full_name" type="text" class="form-control" placeholder="Vendor Full Name..." value="{{ old('full_name', $user->vendor?->full_name) }}" >
        </div>
    </div>

    <!-- Address -->
    <div class="row mb-4">
        <label for="clientDomain" class="col-form-label col-lg-2">{{ __('Address') }}</label>
        <div class="col-lg-10">
            <input id="clientDomain" name="address" type="text" class="form-control" placeholder="Vendor Address..." value="{{ old('address', $user->vendor?->address) }}" >
        </div>
    </div>
</div>

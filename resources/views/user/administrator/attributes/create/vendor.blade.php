<div id="vendorProperties" hidden>
    <!-- Vendor ID -->
    <div class="row mb-4">
        <label for="vendorRemoteId" class="col-form-label col-lg-2">{{ __('Vendor ID') }}</label>
        <div class="col-lg-10">
            <input id="vendorRemoteId" name="vendor_id" type="number" class="form-control" placeholder="Vendor ID">
        </div>
    </div>

    <!-- Full Name -->
    <div class="row mb-4">
        <label for="vendorFullName" class="col-form-label col-lg-2">{{ __('Full Name') }}</label>
        <div class="col-lg-10">
            <input id="vendorFullName" name="full_name" type="text" class="form-control" placeholder="Vendor Full Name">
        </div>
    </div>

    <!-- Address -->
    <div class="row mb-4">
        <label for="vendorAddress" class="col-form-label col-lg-2">{{ __('Address') }}</label>
        <div class="col-lg-10">
            <input id="vendorAddress" name="address" type="text" class="form-control" placeholder="Vendor Address" >
        </div>
    </div>
</div>

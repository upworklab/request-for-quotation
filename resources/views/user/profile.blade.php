@extends('layouts.master')

@section('title') {{ __('User Management') }} @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') <a href="{{ route('user.index') }}">{{ __('User Management') }}</a> @endslot
        @slot('title') {{ __('User Detail') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Edit User') }}</h4>

                    <form method="post" action="{{ route('user.update-profile') }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf

                        <div class="row mb-4">
                            <label for="userName" class="col-form-label col-lg-2">{{ __('Name') }}</label>
                            <div class="col-lg-10">
                                <input id="userName" name="name" type="text" class="form-control" placeholder="Enter User Name..." value="{{ old('name', $user->name) }}">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="userPassword" class="col-form-label col-lg-2">{{ __('Password') }}</label>
                            <div class="col-lg-10">
                                <input id="userPassword" name="password" type="password" class="form-control" placeholder="Enter User Password...">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="userPasswordConfirmation" class="col-form-label col-lg-2">{{ __('Password Confirmation') }}</label>
                            <div class="col-lg-10">
                                <input id="userPasswordConfirmation" name="password_confirmation" type="password" class="form-control" placeholder="Confirm User Password...">
                            </div>
                        </div>

                        <!-- Avatar -->
                        <div class="row mb-4">
                            <label for="avatar" class="col-form-label col-lg-2">{{ __('Avatar') }}</label>
                            <div class="col-lg-10">
                                <a href="{{ $user->avatar_path }}" target="_blank"><img src="{{ $user->avatar_path }}" class="dashboard-avatar my-2"></a>
                                <input id="avatar" name="avatar" type="file" class="form-control" >
                            </div>
                        </div>

                        <!-- Related attributes -->
                        <div id="dynamicProperties">
                            @includeWhen($user->hasRole('administrator'), 'user.administrator.attributes.update.administrator')
                            @includeWhen($user->hasRole('client'), 'user.administrator.attributes.update.client')
                            @includeWhen($user->hasRole('vendor'), 'user.administrator.attributes.update.vendor')
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-lg-10">
                                <a href="{{ route('user.index') }}" role="button" class="btn btn-outline-secondary">{{ __('Cancel') }}</a>
                                <button type="submit" class="btn btn-primary">{{ __('Update Profile') }}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <script>
    $(document).ready(function() {
        $('#dynamicProperties').children('div').attr('hidden', false).find('input').prop('disabled', false);
    });
    </script>

@endsection

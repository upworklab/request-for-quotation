@extends('layouts.master')

@section('title') {{ __('Api Server Management') }} @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') <a href="{{ route('api-server.index') }}">{{ __('Api Server Management') }}</a> @endslot
        @slot('title') {{ __('Create Api Server') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Create New Api Server') }}</h4>

                    <form method="post" action="{{ route('api-server.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row mb-4">
                            <label for="apiServerName" class="col-form-label col-lg-2">{{ __('Name') }}</label>
                            <div class="col-lg-10">
                                <input id="apiServerName" name="name" type="text" class="form-control" placeholder="Enter Api Server Name">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="apiServerBaseUri" class="col-form-label col-lg-2">{{ __('Base URI') }}</label>
                            <div class="col-lg-10">
                                <input id="apiServerBaseUri" name="base_uri" type="text" class="form-control" placeholder="Enter Api Server Base URI">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="apiServerUserName" class="col-form-label col-lg-2">{{ __('Username') }}</label>
                            <div class="col-lg-10">
                                <input id="apiServerUserName" name="username" type="text" class="form-control" placeholder="Enter Api Server Username">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="apiServerPassword" class="col-form-label col-lg-2">{{ __('Password') }}</label>
                            <div class="col-lg-10">
                                <input id="apiServerPassword" name="password" type="text" class="form-control" placeholder="Enter Api Server Password">
                            </div>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-lg-10">
                                <a href="{{ route('api-server.index') }}" role="button" class="btn btn-outline-secondary">Cancel</a>
                                <button type="submit" class="btn btn-primary">Create Api Server</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <script>
    $(document).ready(function() {

    });
    </script>

@endsection

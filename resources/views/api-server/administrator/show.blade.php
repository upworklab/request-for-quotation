@extends('layouts.master')

@section('title') {{ __('Api Server Management') }} @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('actionButtons')
            @can('delete', $api_server)
            <form method="post" action="{{ route('api-server.destroy', $api_server) }}">
                @method('DELETE')
                @csrf
                <button class="btn btn-danger" type="submit">
                    <i class="bx bx-trash me-1"></i> {{ __('Delete') }}
                </button>
            </form>
            @endcan
        @endslot
        @slot('li_1') <a href="{{ route('api-server.index') }}">{{ __('Api Server Management') }}</a> @endslot
        @slot('title') {{ __('Edit Api Server') }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <x-validation-errors :errors="$errors" />

                    <h4 class="card-title mb-4">{{ __('Api Server Detail') }}</h4>

                    <form method="post" action="{{ route('api-server.update', $api_server) }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf

                        <div class="row mb-4">
                            <label for="apiServerName" class="col-form-label col-lg-2">{{ __('Name') }}</label>
                            <div class="col-lg-10">
                                <input id="apiServerName" name="name" type="text" class="form-control" placeholder="Enter Api Server Name" value="{{ old('name', $api_server->name) }}">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="apiServerBaseUri" class="col-form-label col-lg-2">{{ __('Base URI') }}</label>
                            <div class="col-lg-10">
                                <input id="apiServerBaseUri" name="base_uri" type="text" class="form-control" placeholder="Enter Api Server Base URI" value="{{ old('base_uri', $api_server->base_uri) }}">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="apiServerUserName" class="col-form-label col-lg-2">{{ __('Username') }}</label>
                            <div class="col-lg-10">
                                <input id="apiServerUserName" name="username" type="text" class="form-control" placeholder="Enter Api Server Username" value="{{ old('username', $api_server->username) }}">
                            </div>
                        </div>

                        <div class="row mb-4">
                            <label for="apiServerPassword" class="col-form-label col-lg-2">{{ __('Password') }}</label>
                            <div class="col-lg-10">
                                <input id="apiServerPassword" name="password" type="text" class="form-control" placeholder="Enter Api Server Password" value="{{ old('password', $api_server->password) }}">
                            </div>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-lg-10">
                                <a href="{{ route('api-server.index') }}" role="button" class="btn btn-outline-secondary">{{ __('Cancel') }}</a>
                                <button type="submit" class="btn btn-primary">{{ __('Update Api Server') }}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <script>
    $(document).ready(function() {

    });
    </script>

@endsection

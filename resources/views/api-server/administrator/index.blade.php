@extends('layouts.master')

@section('title') Api Server Management @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('actionButtons')
        <a href="{{ route('api-server.create') }}" class="btn btn-primary" role="button">
            <i class="bx bx-plus me-1"></i> Add Api Server
        </a>
        @endslot
        @slot('li_1') Dashboard @endslot
        @slot('title') Api Server Management @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <!-- <h4 class="card-title">Api Server List</h4> -->
                    <!-- <p class="card-title-desc"></p> -->

                    <table id="dataTable" class="table table-bordered dt-responsive  nowrap w-100">
                        <thead>
                            <tr>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Base URI') }}</th>
                                <th>{{ __('Username') }}</th>
                                <th>{{ __('Password') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/libs/pdfmake/pdfmake.min.js') }}"></script>

    <script src="{{ asset('js/datatables/plugin.js') }}"></script>
    <script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
    <script>
    $(document).ready(function() {

        var table = $('#dataTable').DataTable({
            autoWidth: false,
            // lengthChange: false,
            searching: true,
            processing: true,
            serverSide: true,
            responsive: true,
            order: [[0, "desc"]],
            ajax: {
                url: '{{ route("api-server.data") }}',
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
                statusCode: {
                    403: function() {
                        window.location.href = '{{ route("home") }}';
                    }
                }
            },
            columns: [
                { data: 'name' },
                { data: 'base_uri' },
                { data: 'username' },
                { data: 'password' },
                {
                    sortable: false,
                    data: 'id',
                    render: function ( data, type, row, meta ) {
                        // var buttonClass = "inline-flex items-center px-4 py-2 rounded-lg font-semibold text-xs uppercase tracking-widest text-white focus:outline-none bg-indigo-500 border border-indigo-500 hover:bg-indigo-400 hover:border-indigo-500 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700 transition ease-in-out duration-150";
                        var buttonClass = "btn btn-primary btn-sm";
                        var targetUrl = "{{ route('api-server.index') }}/"+data;
                        var button = '<a href="'+targetUrl+'" class="'+buttonClass+'" data-uuid="'+data+'">View</a>';
                        return button;
                    },
                },
            ]
        });

        $(".dataTables_length select").addClass('form-select form-select-sm');

        $('#searchTable').keyup(delayResponse(function (e) {
            console.log(this.value);
            table.search( this.value ).draw();
        }, 500));

    });
    </script>

@endsection

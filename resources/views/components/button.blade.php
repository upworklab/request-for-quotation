<button {{ $attributes->merge(['type' => 'submit', 'class' => 'inline-flex items-center px-4 py-2 rounded-lg font-semibold text-xs uppercase tracking-widest text-white focus:outline-none bg-indigo-500 border border-indigo-500 hover:bg-indigo-400 hover:border-indigo-500 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700 transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>

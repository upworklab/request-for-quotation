@if (session('success'))
<div {{ $attributes->merge(['class' => 'alert alert-success alert-dismissible fade show']) }} role="alert">
    <i class="mdi mdi-check me-2"></i>
    {{ session('success') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session('warning'))
<div {{ $attributes->merge(['class' => 'alert alert-warning alert-dismissible fade show']) }} role="alert">
    <i class="mdi mdi-exclamation-thick me-2"></i>
    {{ session('warning') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

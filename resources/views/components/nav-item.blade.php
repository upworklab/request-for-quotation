@props(['active'])

@php
$classes = ($active ?? false)
            ? 'mm-active'
            : 'mm-inactive';
@endphp

<li {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</li>

@extends('layouts.master-without-nav')

@section('title')
    {{ __('Register') }}
@endsection

@section('body')

<body>
@endsection

@section('content')
    <div class="account-pages my-5 pt-sm-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">
                        <div class="bg-primary bg-soft">
                            <div class="row">
                                <div class="col-7">
                                    <div class="text-primary p-4">
                                        <h5 class="text-primary">Registration</h5>
                                        <p>Get your RFQ App account.</p>
                                    </div>
                                </div>
                                <div class="col-5 align-self-end">
                                    <img src="{{ asset('assets/images/profile-img.png') }}" alt=""
                                        class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="auth-logo">
                                <a href="" class="auth-logo-light">
                                    <div class="avatar-md profile-user-wid mb-4">
                                        <span class="avatar-title rounded-circle bg-light">
                                            <img src="{{ asset('assets/images/logo-light.svg') }}" alt=""
                                                class="rounded-circle" height="34">
                                        </span>
                                    </div>
                                </a>

                                <a href="" class="auth-logo-dark">
                                    <div class="avatar-md profile-user-wid mb-4">
                                        <span class="avatar-title rounded-circle bg-light">
                                            <img src="{{ asset('assets/images/logo.svg') }}" alt=""
                                                class="rounded-circle" height="34">
                                        </span>
                                    </div>
                                </a>
                            </div>
                            <div class="p-2">
                                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                    @csrf

                                    <div class="mb-3">
                                        <label for="company_legal_name" class="form-label">{{ __('Company Legal Name') }}</label>
                                        <input name="company_legal_name" type="text"
                                            class="form-control @error('company_legal_name') is-invalid @enderror"
                                            value="{{ old('company_legal_name') }}" id="company_legal_name"
                                            required="required"
                                            placeholder="{{ __('Enter Company Legal Name') }}" autofocus>
                                        @error('company_legal_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="year_of_establishment" class="form-label">{{ __('Year of Establishment') }}</label>
                                        <input name="year_of_establishment" type="text"
                                            class="form-control @error('year_of_establishment') is-invalid @enderror"
                                            value="{{ old('year_of_establishment') }}" id="year_of_establishment"
                                            required="required"
                                            placeholder="{{ __('Enter Year of Establishment') }}">
                                        @error('year_of_establishment')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="referral_code" class="form-label">{{ __('Referral Code') }}</label>
                                        <input name="referral_code" type="text"
                                            class="form-control @error('referral_code') is-invalid @enderror"
                                            value="{{ old('referral_code') }}" id="referral_code"
                                            placeholder="{{ __('Enter Referral Code') }}">
                                        @error('referral_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="contact_name" class="form-label">{{ __('Contact Name') }}</label>
                                        <input name="contact_name" type="text"
                                            class="form-control @error('contact_name') is-invalid @enderror"
                                            value="{{ old('contact_name') }}" id="contact_name"
                                            required="required"
                                            placeholder="{{ __('Enter Contact Name') }}">
                                        @error('contact_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="phone" class="form-label">{{ __('Phone') }}</label>
                                        <input name="phone" type="text"
                                            class="form-control @error('phone') is-invalid @enderror"
                                            value="{{ old('phone') }}" id="phone"
                                            placeholder="{{ __('Enter Phone') }}">
                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="address" class="form-label">{{ __('Address') }}</label>
                                        <input name="address" type="text"
                                            class="form-control @error('address') is-invalid @enderror"
                                            value="{{ old('address') }}" id="address"
                                            placeholder="{{ __('Enter Address') }}">
                                        @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="username" class="form-label">{{ __('Email') }}</label>
                                        <input name="email" type="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            value="{{ old('email') }}" id="username"
                                            placeholder="{{ __('Enter Email') }}" autocomplete="email">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">{{ __('Password') }}</label>
                                        <div
                                            class="input-group auth-pass-inputgroup @error('password') is-invalid @enderror">
                                            <input type="password" name="password"
                                                class="form-control  @error('password') is-invalid @enderror"
                                                id="userpassword" placeholder="{{ __('Enter password') }}"
                                                aria-label="Password" aria-describedby="password-addon">
                                            <button class="btn btn-light password-addon " type="button"><i class="mdi mdi-eye-outline"></i></button>
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">{{ __('Confirm Password') }}</label>
                                        <div
                                            class="input-group auth-pass-inputgroup @error('password') is-invalid @enderror">
                                            <input type="password" name="password_confirmation"
                                                class="form-control  @error('password') is-invalid @enderror"
                                                id="userpassword_confirmation" placeholder="{{ __('Confirm Password') }}"
                                                aria-label="Confirm Password" aria-describedby="password-addon">
                                            <button class="btn btn-light password-addon " type="button"><i class="mdi mdi-eye-outline"></i></button>
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label for="captcha" class="form-label">Captcha</label>
                                        <div class="mb-2">{!!  Captcha::img() !!}</div>
                                        <input name="captcha" type="text"
                                            class="form-control @error('captcha') is-invalid @enderror"
                                            value="{{ old('captcha') }}" id="captcha"
                                            placeholder="Enter Captcha" autocomplete="off" autofocus>
                                        @error('captcha')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ __('Incorrect Captcha') }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="mt-3 d-grid">
                                        <button class="btn btn-primary waves-effect waves-light" type="submit">{{ __('Register') }}</button>
                                    </div>

                                    <div class="mt-4 text-center">
                                        @if (Route::has('password.request'))
                                            <a href="{{ route('password.request') }}" class="text-muted"><i
                                                    class="mdi mdi-lock me-1"></i> Forgot your password?</a>
                                        @endif

                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="mt-5 text-center">

                        <div>
                            <p>Already registered ? <a href="{{ route('login') }}" class="fw-medium text-primary">
                                    Login </a> </p>
                            <p>&copy; <script>
                                    document.write(new Date().getFullYear())

                                </script> RFQ App
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- end account-pages -->

@endsection

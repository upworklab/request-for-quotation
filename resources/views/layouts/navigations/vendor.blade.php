@if(Auth::user()->hasRole('vendor'))
    @if ($responsive)
        @php
        $tag = 'responsive-nav-link';
        @endphp
    @else
        @php
        $tag = 'nav-link';
        @endphp
    @endif
    <x-dynamic-component :component="$tag" :href="route('home')" :active="request()->routeIs('home')">
        {{ __('Home') }}
    </x-dynamic-component>
    <x-dynamic-component :component="$tag" :href="route('purchase-request.index')" :active="request()->routeIs('purchase-request*')">
        {{ __('Purchase Request') }}
    </x-dynamic-component>
    <x-dynamic-component :component="$tag" :href="route('quotation.index')" :active="request()->routeIs('quotation*')">
        {{ __('Quotation') }}
    </x-dynamic-component>
@endif

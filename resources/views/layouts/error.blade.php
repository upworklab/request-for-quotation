<div class="mx-auto w-192 bg-red-600 drop-shadow-lg rounded-xl">
    <div class="px-16">
        <div class="py-10 text-center text-white">
            <p class="text-xl font-semibold">Error</p>
            <p class="">{{ $error }}</p>
        </div>
    </div>
</div>

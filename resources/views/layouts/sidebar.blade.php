<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu">{{ __('Menu') }}</li>

                <x-nav-item :active="request()->routeIs('home')">
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span key="t-dashboards">{{ __('Home') }}</span>
                    </a>
                </x-nav-item>

                @can('viewAny', App\Models\User::class)
                <x-nav-item :active="request()->routeIs('user.*')" class="">
                    <a href="{{ route('user.index') }}" class="waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span key="t-user">{{ __('User') }}</span>
                    </a>
                </x-nav-item>
                @endcan

                @can('viewAny', App\Models\PurchaseRequest::class)
                <x-nav-item :active="request()->routeIs('purchase-request.*')">
                    <a href="{{ route('purchase-request.index') }}" class="waves-effect">
                        <i class="bx bx-cart"></i>
                        <span key="t-purchase-request">{{ __('Purchase Request') }}</span>
                    </a>
                </x-nav-item>
                @endcan

                @can('viewAny', App\Models\Quotation::class)
                <x-nav-item :active="request()->routeIs('quotation.*')">
                    <a href="{{ route('quotation.index') }}" class="waves-effect">
                        <i class="bx bx-spreadsheet"></i>
                        <span key="t-quotation">{{ __('Quotation') }}</span>
                    </a>
                </x-nav-item>
                @endcan

                @can('viewAny', App\Models\ApiServer::class)
                <x-nav-item :active="request()->routeIs('api-server.*')">
                    <a href="{{ route('api-server.index') }}" class="waves-effect">
                        <i class="bx bx-plug"></i>
                        <span key="t-api-server">{{ __('Api Server') }}</span>
                    </a>
                </x-nav-item>
                @endcan

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->

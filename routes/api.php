<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')->group(function () {
    // Route::get('products', [App\Http\Controllers\Api\ProductController::class, 'index'])->name('api.products.index');
    Route::get('products/search', [App\Http\Controllers\Api\ProductController::class, 'search'])->name('api.products.search');
    Route::get('uom/search', [App\Http\Controllers\Api\UomController::class, 'search'])->name('api.uoms.search');
});

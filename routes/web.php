<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the 'web' middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('profile', [App\Http\Controllers\UserController::class, 'showProfile'])->name('user.profile');
    Route::get('user-data', [App\Http\Controllers\UserController::class, 'getDatatables'])->name('user.data');
    Route::patch('profile', [App\Http\Controllers\UserController::class, 'updateProfile'])->name('user.update-profile');
    Route::resource('user', App\Http\Controllers\UserController::class);

    Route::get('purchase-request-data', [App\Http\Controllers\PurchaseRequestController::class, 'getDatatables'])->name('purchase-request.data');
    Route::patch('purchase-request/{purchase_request}/finalize', [App\Http\Controllers\PurchaseRequestController::class, 'finalize'])->name('purchase-request.finalize');
    Route::resource('purchase-request', App\Http\Controllers\PurchaseRequestController::class);

    Route::get('purchase-request/{purchase_request}/quotation/create', [App\Http\Controllers\QuotationController::class, 'create'])->name('purchase-request.create-quotation');
    Route::post('purchase-request/{purchase_request}/quotation/store', [App\Http\Controllers\QuotationController::class, 'store'])->name('purchase-request.store-quotation');
    Route::get('purchase-request/{purchase_request}/quotation/index', [App\Http\Controllers\QuotationController::class, 'index'])->name('purchase-request.index-quotation');
    Route::get('purchase-request/{purchase_request}/quotation/datatable', [App\Http\Controllers\QuotationController::class, 'getDatatables'])->name('purchase-request.datatable-quotation');

    Route::get('quotation/{quotation}', [App\Http\Controllers\QuotationController::class, 'show'])->name('quotation.show');
    Route::get('quotation', [App\Http\Controllers\QuotationController::class, 'index'])->name('quotation.index');
    Route::get('quotation-data', [App\Http\Controllers\QuotationController::class, 'getDatatables'])->name('quotation.data');
    Route::patch('quotation/{quotation}', [App\Http\Controllers\QuotationController::class, 'update'])->name('quotation.update');
    Route::patch('quotation/{quotation}/finalize', [App\Http\Controllers\QuotationController::class, 'finalize'])->name('quotation.finalize');

    Route::get('api-server-data', [App\Http\Controllers\ApiServerController::class, 'getDatatables'])->name('api-server.data');
    Route::resource('api-server', App\Http\Controllers\ApiServerController::class);
});
// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

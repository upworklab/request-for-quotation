<?php

return [
    'user_statuses' => [
        'PENDING_VERIFICATION',
        'VERIFIED',
        'ACTIVE',
        'DEACTIVATED',
    ],
    'purchase_request_statuses' => [
        'DRAFT',
        'SENT',
        'ACTIVE',
        'CLOSED',
    ],
    'quotation_statuses' => [
        'DRAFT',
        'SENT',
        'PASSED',
        'PURCHASED',
    ],
];

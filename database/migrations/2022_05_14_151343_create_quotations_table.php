<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('location_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('purchase_code');
            $table->date('expected_date');
            $table->string('remark')->nullable();
            $table->enum('status', config('constants.purchase_request_statuses'))->default(config('constants.purchase_request_statuses')[0]);
            $table->unsignedBigInteger('remote_id')->nullable();
            $table->text('remote_requisition')->nullable();
            $table->unsignedBigInteger('remote_requisition_id')->nullable();
            $table->unsignedBigInteger('remote_partner_id')->nullable();
            $table->date('remote_valid_until')->nullable();
            $table->enum('remote_status', config('constants.purchase_request_statuses'))->default(config('constants.purchase_request_statuses')[0]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
};

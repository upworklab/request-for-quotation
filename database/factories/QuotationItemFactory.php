<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\QuotationItem>
 */
class QuotationItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'quotation_id' => \App\Models\Quotation::factory(),
            'product_id' => \App\Models\Product::factory(),
            'uom_id' => \App\Models\Uom::factory(),
            'quantity' => rand(10,100),
            'quoted_price' => rand(10000,1000000),
        ];
    }
}

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PurchaseRequestItem>
 */
class PurchaseRequestItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'purchase_request_id' => \App\Models\PurchaseRequest::factory(),
            'product_id' => \App\Models\Product::factory(),
            'uom_id' => \App\Models\Uom::factory(),
            // 'product_name' => $this->faker->colorName,
            // 'product_description' => $this->faker->sentence(7),
            // 'product_remark' => $this->faker->sentence(11),
            'quantity' => rand(10,100),
            'quoted_price' => rand(10000,1000000),
            // 'uom' => \Illuminate\Support\Arr::random(['Unit', 'Litre', 'Ton', 'Meter']),
        ];
    }
}

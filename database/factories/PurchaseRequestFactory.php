<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PurchaseRequest>
 */
class PurchaseRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'client_id' => \App\Models\Client::factory(),
            'location_id' => \App\Models\Location::factory(),
            // 'delivery_location' =>  $this->faker->city(),
            'purchase_code' => $this->faker->countryCode() . $this->faker->currencyCode(),
            'status' => config('constants.purchase_request_statuses')[array_rand(config('constants.purchase_request_statuses'))],
            'expected_date' => now()->addDays(rand(1,3))->format('d/m/Y'),
            'remark' => $this->faker->sentence(7),
        ];
    }
}

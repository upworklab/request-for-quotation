<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Client;
use App\Models\PurchaseRequest;
use App\Models\PurchaseRequestItem;

class PurchaseRequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = Client::all();
        foreach ($clients as $client) {
            PurchaseRequest::factory()
                ->count(3)
                ->hasItems(5)
                ->create([
                    'client_id' => $client->id,
                ]);
        }
    }
}

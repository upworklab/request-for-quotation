<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $roles = Role::where('name', 'administrator')->get();

        // $roles = Role::all();
        foreach ($roles as $role) {
            $related_role = '\\App\Models\\'.ucwords($role->name);
            User::factory()
                ->hasAttached($role)
                ->has(
                    $related_role::factory()->count(1)
                )
                ->create([
                    'name' => ucfirst($role->name),
                    'email' => $role->name.'@example.test',
                ]);
        }
    }
}

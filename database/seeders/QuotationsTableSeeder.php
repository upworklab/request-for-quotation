<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Vendor;
use App\Models\Quotation;
use App\Models\PurchaseRequest;

class QuotationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $purchase_requests = PurchaseRequest::all();
        $vendors = Vendor::all();
        foreach ($purchase_requests as $purchase_request) {
            foreach ($vendors as $vendor) {
                Quotation::factory()
                    ->count(1)
                    ->hasItems(3)
                    ->create([
                        'purchase_request_id' => $purchase_request->id,
                        'vendor_id' => $vendor->id,
                    ]);
            }
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\Models\Role;
        $role->name = 'administrator';
        $role->description = 'Website administrator';
        $role->save();

        $role = new \App\Models\Role;
        $role->name = 'client';
        $role->description = 'Purchasing Client';
        $role->save();

        $role = new \App\Models\Role;
        $role->name = 'vendor';
        $role->description = 'Purchasing Vendor';
        $role->save();

    }
}

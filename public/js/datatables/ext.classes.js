// TODO: Move most of the datatables.custom.css here
(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery', 'datatables.net'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				root = window;
			}

			if ( ! $ || ! $.fn.dataTable ) {
				// Require DataTables, which attaches to jQuery, including
				// jQuery if needed and have a $ property so we can access the
				// jQuery object that is used
				$ = require('datatables.net')(root, $).$;
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document, undefined ) {
	'use strict';
	var DataTable = $.fn.dataTable;

	$.extend( true, DataTable.defaults, {
		dom:
				"<'flex'<'flex-1'tr>>" +
				"<'flex'<'w-1/2'l><'w-1/2 float-right'f>>" +
				"<'flex items-center'<'w-1/2 p-4'i><'w-1/2 p-4 float-right'p>>",
		renderer: 'tailwindCss'
	} );

	$.extend( DataTable.ext.classes, {
		"sTable": "dataTable w-full text-sm text-left",
		"sNoFooter": "no-footer",

		/* Paging buttons */
		"sPageButton": "paginate_button text-xs font-bold",
		"sPageButtonActive": "current",
		"sPageButtonDisabled": "disabled",

		/* Striping classes */
		"sStripeOdd": "border-b dark:border-gray-700 bg-white dark:bg-gray-800",
		"sStripeEven": "border-b dark:border-gray-700 bg-gray-50 dark:bg-gray-700",

		/* Empty row */
		"sRowEmpty": "dataTables_empty",

		/* Features */
		"sWrapper": "dataTables_wrapper h-full",
		"sFilter": "dataTables_filter hidden",
		"sInfo": "dataTables_info clear-both float-left text-sm",
		"sPaging": "dataTables_paginate paging_", /* Note that the type is postfixed */
		"sLength": "dataTables_length",
		"sProcessing": "dataTables_processing w-full absolute text-center bg-white top-2/4",

		/* Sorting */
		"sSortAsc": "sorting_asc",
		"sSortDesc": "sorting_desc",
		"sSortable": "sorting", /* Sortable in both directions */
		"sSortableAsc": "sorting_desc_disabled",
		"sSortableDesc": "sorting_asc_disabled",
		"sSortableNone": "sorting_disabled",
		"sSortColumn": "sorting_", /* Note that an int is postfixed for the sorting order */

		/* Filtering */
		"sFilterInput": "",

		/* Page length */
		"sLengthSelect": "",

		/* Scrolling */
		"sScrollWrapper": "dataTables_scroll",
		"sScrollHead": "dataTables_scrollHead",
		"sScrollHeadInner": "dataTables_scrollHeadInner",
		"sScrollBody": "dataTables_scrollBody",
		"sScrollFoot": "dataTables_scrollFoot",
		"sScrollFootInner": "dataTables_scrollFootInner",

		/* Misc */
		"sHeaderTH": "text-black font-bold p-4",
		"sFooterTH": "",
	} );

	DataTable.ext.renderer.pageButton.tailwindCss = function ( settings, host, idx, buttons, page, pages ) {
		var api     = new DataTable.Api( settings );
		var classes = settings.oClasses;
		var lang    = settings.oLanguage.oPaginate;
		var aria = settings.oLanguage.oAria.paginate || {};
		var btnDisplay, btnClass, counter=0;

		var attach = function( container, buttons ) {
			var i, ien, node, button;
			var clickHandler = function ( e ) {
				e.preventDefault();
				if ( !$(e.currentTarget).hasClass('disabled') && api.page() != e.data.action ) {
					api.page( e.data.action ).draw( 'page' );
				}
			};

			for ( i=0, ien=buttons.length ; i<ien ; i++ ) {
				button = buttons[i];

				if ( Array.isArray( button ) ) {
					attach( container, button );
				}
				else {
					btnDisplay = '';
					btnClass = '';

					switch ( button ) {
						case 'ellipsis':
							btnDisplay = '&#x2026;';
							btnClass = 'disabled';
							break;

						case 'first':
							btnDisplay = lang.sFirst;
							btnClass = button + (page > 0 ?
								'' : ' disabled');
							break;

						case 'previous':
							btnDisplay = lang.sPrevious;
							btnClass = button + (page > 0 ?
								' border-l border-t border-b rounded-l-md' : ' border-l border-t border-b rounded-l-md disabled');
							break;

						case 'next':
							btnDisplay = lang.sNext;
							btnClass = button + (page < pages-1 ?
								' border rounded-r-md' : ' border rounded-r-md disabled');
							break;

						case 'last':
							btnDisplay = lang.sLast;
							btnClass = button + (page < pages-1 ?
								'' : ' disabled');
							break;

						default:
							btnDisplay = button + 1;
							btnClass = page === button ?
								'border-l border-t border-b current' : ' border-l border-t border-b';
							break;
					}

					if ( btnDisplay ) {
						node = $('<li>', {
								'class': classes.sPageButton+' '+btnClass,
								'id': idx === 0 && typeof button === 'string' ?
									settings.sTableId +'_'+ button :
									null
							} )
							.append( $('<a>', {
									'href': '#',
									'aria-controls': settings.sTableId,
									'aria-label': aria[ button ],
									'data-dt-idx': counter,
									'tabindex': settings.iTabIndex,
									'class': 'block px-4 py-2 '
								} )
								.html( btnDisplay )
							)
							.appendTo( container );

						settings.oApi._fnBindAction(
							node, {action: button}, clickHandler
						);

						counter++;
					}
				}
			}
		};

		// IE9 throws an 'unknown error' if document.activeElement is used
		// inside an iframe or frame.
		var activeEl;

		try {
			// Because this approach is destroying and recreating the paging
			// elements, focus is lost on the select button which is bad for
			// accessibility. So we want to restore focus once the draw has
			// completed
			activeEl = $(host).find(document.activeElement).data('dt-idx');
		}
		catch (e) {}

		attach(
			$(host).empty().html('<ul class="flex float-right"/>').children('ul'),
			buttons
		);

		if ( activeEl !== undefined ) {
			$(host).find( '[data-dt-idx='+activeEl+']' ).trigger('focus');
		}
	};

	return DataTable;
}));

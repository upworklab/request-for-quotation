<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\UsesRemoteId;

class PurchaseRequestItem extends Model
{
    use HasFactory, UsesRemoteId;

    protected $guarded = ['id'];

    public function purchaseRequest()
    {
        return $this->belongsTo(\App\Models\PurchaseRequest::class);
    }

    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class);
    }

    public function uom()
    {
        return $this->belongsTo(\App\Models\Uom::class);
    }
}

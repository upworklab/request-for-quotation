<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\UsesRemoteId;

class Product extends Model
{
    use HasFactory, UsesRemoteId;
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\UsesRemoteId;

class Vendor extends Model
{
    use HasFactory, UsesRemoteId;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function quotations()
    {
        // return $this->hasMany(\App\Models\PurchaseRequest::class, 'remote_partner_id', 'remote_id');
        return $this->hasMany(\App\Models\Quotation::class, 'remote_partner_id', 'remote_id');
    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', ['ACTIVE', 'VERIFIED']);
    }
}

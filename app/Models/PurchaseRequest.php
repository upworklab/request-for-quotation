<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use App\Models\Traits\UsesRemoteId;

class PurchaseRequest extends Model
{
    use HasFactory, UsesRemoteId;

    protected $guarded = ['id'];
    // protected $dates = ['expected_date'];

    public function expectedDate(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y'),
            set: fn ($value) => Carbon::createFromFormat('d/m/Y', $value),
        );
    }

    public function remoteValidUntil(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value ? Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y') : null,
            // get: fn ($value) => Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y'),
            set: fn ($value) => Carbon::createFromFormat('d/m/Y', $value),
        );
    }

    public function requisition(): Attribute
    {
        return Attribute::make(
            get: fn($value, $attributes) => $attributes['remote_requisition'],
            set: fn($value) => [
                'remote_requisition' => $value,
            ],
        );
    }

    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class);
    }

    public function location()
    {
        return $this->belongsTo(\App\Models\Location::class);
    }

    public function items()
    {
        return $this->hasMany(\App\Models\PurchaseRequestItem::class);
    }

    public function quotations()
    {
        return $this->hasMany(\App\Models\Quotation::class);
    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', ['SENT', 'DRAFT']);
    }

    public function scopeOfClient($query, \App\Models\Client $client)
    {
        return $query->where('client_id', $client->id);
    }

    public function scopeHasRequisition($query)
    {
        // return $query->where('remote_requisition', '<>', '');
        return $query->whereNotNull('remote_requisition');
    }

    public function scopeHasSubmittedWithPartner($query, $remotePartnerId)
    {
        return $query->where('remote_partner_id', $remotePartnerId);
    }

    public function scopeHasSubmittedWithRequisitionAndPartner($query, $remoteRequisitionId, $remotePartnerId)
    {
        return $query->where('remote_partner_id', $remotePartnerId)
            ->where('remote_requisition_id', $remoteRequisitionId)
            ->firstOr(function() {
                return false;
            });
    }
}

<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait UsesRemoteId
{
    public static function findRemoteId($id)
    {
        return static::firstWhere('remote_id', $id);
    }
}

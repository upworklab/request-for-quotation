<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\UsesRemoteId;

class Client extends Model
{
    use HasFactory, UsesRemoteId;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function purchaseRequests()
    {
        return $this->hasMany(\App\Models\PurchaseRequest::class);
    }

}

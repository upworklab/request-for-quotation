<?php

namespace App\Http\Requests\PurchaseRequest;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\PurchaseRequest;
use Yajra\Datatables\Datatables;

class ShowPurchaseRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("view", $this->route("purchase_request"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill(PurchaseRequest $purchaseRequest)
    {
        if ($this->user()->administrator) {
            return view('purchase-request.administrator.show')
                ->with('purchase_request', $purchaseRequest)
                ->with('clients', \App\Models\Client::all());

        } elseif ($this->user()->vendor) {
            return view('purchase-request.vendor.show')
                ->with('purchase_request', $purchaseRequest);
        }
    }
}

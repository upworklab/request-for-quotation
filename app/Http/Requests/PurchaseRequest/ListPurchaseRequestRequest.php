<?php

namespace App\Http\Requests\PurchaseRequest;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Quotation;
use App\Models\PurchaseRequest;
use Yajra\Datatables\Datatables;

class ListPurchaseRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("viewAny", PurchaseRequest::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill()
    {
        if ($this->user()->administrator) {
            $model = PurchaseRequest::with(['client', 'location'])->hasRequisition()->select('purchase_requests.*');

            return Datatables::of($model)->make(true);

        } elseif ($this->user()->vendor) {
            $model = PurchaseRequest::active()->with(['client', 'location'])->hasRequisition()->select('purchase_requests.*');
            $submittedQuotation = Quotation::hasSubmittedWithPartner($this->user()->vendor->remote_id)->get();
            // dd($submittedQuotation->firstWhere('remote_requisition_id', 1743434));

            return Datatables::of($model)
                ->addColumn('adjusted_status', function($quotation) use ($submittedQuotation) {
                    // return in_array($quotation->remote_requisition_id, $submittedQuotation);
                    $submitted = $submittedQuotation->firstWhere('remote_requisition_id', $quotation->remote_requisition_id);
                    return $submitted ? $submitted->status : 'OPEN';
                })
                ->make(true);

        } elseif ($this->user()->client) {
            $model = PurchaseRequest::ofClient($this->user()->client)->with(['client', 'location'])->hasRequisition()->select('purchase_requests.*');
        }


    }
}

<?php

namespace App\Http\Requests\PurchaseRequest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use DB;
use App\Models\PurchaseRequest;
use App\Models\PurchaseRequestItem;

class UpdatePurchaseRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("update", $this->route("purchase_request"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company' => [
                'required',
                'exists:App\Models\Client,id',
            ],
            'purchase_code' => [
                'required',
                'max:255',
            ],
            'delivery_location' => [
                'required',
                'max:255',
            ],
            'expected_date' => [
                'required',
                'date_format:d/m/Y',
            ],
            'remark' => [
                'nullable',
                'string',
            ],
            'product_name.*' => [
                'nullable',
                'string',
            ],
            'product_description.*' => [
                'nullable',
                'string',
            ],
            'quantity.*' => [
                'nullable',
                'string',
            ],
            'uom.*' => [
                'nullable',
                'string',
            ],
        ];
    }

    public function validatedData()
    {
        $validated = parent::validated();
        $model_attributes = \Illuminate\Support\Arr::except($validated, 'company');
        return array_merge($model_attributes, [
            "client_id" => $validated['company'],
        ]);
    }

    public function fulfill(PurchaseRequest $purchaseRequest)
    {
        DB::beginTransaction();

        try {
            // Update model
            $validated_data = $this->validatedData();
            $purchaseRequest->update($validated_data);
            $purchaseRequest->items()->delete();
            foreach ($validated_data['product_name'] as $index => $item) {
                if (($validated_data['product_description'][$index] != "")
                    AND ($validated_data['quantity'][$index] != "")
                    AND ($validated_data['uom'][$index] != "")) {
                        $item = new PurchaseRequestItem([
                            'product_name' => $validated_data['product_name'][$index],
                            'product_description' => $validated_data['product_description'][$index],
                            'quantity' => $validated_data['quantity'][$index],
                            'uom' => $validated_data['uom'][$index],
                        ]);
                        $purchaseRequest->items()->save($item);
                }
            }

            DB::commit();

            return $purchaseRequest;

        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            abort(500);
            // return false;
        }
    }
}

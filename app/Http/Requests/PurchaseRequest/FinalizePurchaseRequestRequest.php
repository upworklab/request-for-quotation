<?php

namespace App\Http\Requests\PurchaseRequest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use DB;
use App\Models\PurchaseRequest;
use App\Models\PurchaseRequestItem;

class FinalizePurchaseRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("update", $this->route("purchase_request"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function fulfill(PurchaseRequest $purchaseRequest)
    {
        $purchaseRequest->status = 'ACTIVE';
        $purchaseRequest->save();
    }
}

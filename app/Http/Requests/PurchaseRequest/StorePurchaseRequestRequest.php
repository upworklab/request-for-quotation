<?php

namespace App\Http\Requests\PurchaseRequest;

use Illuminate\Foundation\Http\FormRequest;
use DB;
use App\Models\PurchaseRequest;
use App\Models\PurchaseRequestItem;

class StorePurchaseRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("create", PurchaseRequest::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company' => [
                'required',
                'exists:App\Models\Client,id',
            ],
            'purchase_code' => [
                'required',
                'max:255',
            ],
            'delivery_location' => [
                'required',
                'max:255',
            ],
            'expected_date' => [
                'required',
                'date_format:d/m/Y',
            ],
            'remark' => [
                'nullable',
                'string',
            ],
            'product_name.*' => [
                'nullable',
                'string',
            ],
            'product_description.*' => [
                'nullable',
                'string',
            ],
            'quantity.*' => [
                'nullable',
                'string',
            ],
            'uom.*' => [
                'nullable',
                'string',
            ],
        ];
    }

    public function validatedData()
    {
        $validated = parent::validated();
        $model_attributes = \Illuminate\Support\Arr::except($validated, 'company');
        return array_merge($model_attributes, [
            'client_id' => $validated['company'],
        ]);
    }

    public function fulfill()
    {
        DB::beginTransaction();

        try {
            // Create model
            $validated_data = $this->validatedData();
            $model = PurchaseRequest::create($validated_data);
            foreach ($validated_data['product_name'] as $index => $item) {
                if (($validated_data['product_description'][$index] != "")
                    AND ($validated_data['quantity'][$index] != "")
                    AND ($validated_data['uom'][$index] != "")) {
                        $item = new PurchaseRequestItem([
                            'product_name' => $validated_data['product_name'][$index],
                            'product_description' => $validated_data['product_description'][$index],
                            'quantity' => $validated_data['quantity'][$index],
                            'uom' => $validated_data['uom'][$index],
                        ]);
                        $model->items()->save($item);
                }
            }

            DB::commit();

            return $model;

        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            dd($e);
            abort(500);
            // return false;
        }
    }
}

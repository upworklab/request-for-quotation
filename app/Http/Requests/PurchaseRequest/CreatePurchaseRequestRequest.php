<?php

namespace App\Http\Requests\PurchaseRequest;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\PurchaseRequest;

class CreatePurchaseRequestRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user()->can("create", PurchaseRequest::class);
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill()
    {
        if ($this->user()->administrator) {
            return view('purchase-request.administrator.create')
                ->with('clients', \App\Models\Client::all());

        } elseif ($this->user()->client) {
            return view('purchase-request.client.create');
        }
    }
}

<?php

namespace App\Http\Requests\PurchaseRequest;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\PurchaseRequest;

class IndexPurchaseRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("viewAny", PurchaseRequest::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill()
    {
        if ($this->user()->administrator) {
            return view('purchase-request.administrator.index')
                ->with('clients', \App\Models\Client::all());

        } elseif ($this->user()->vendor) {
            return view('purchase-request.vendor.index');
        }
    }
}

<?php

namespace App\Http\Requests\ApiServer;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\ApiServer;
use Yajra\Datatables\Datatables;

class ShowApiServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("view", $this->route('api_server'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill(ApiServer $apiServer)
    {
        return view('api-server.administrator.show')
            ->with('api_server', $apiServer);
    }
}

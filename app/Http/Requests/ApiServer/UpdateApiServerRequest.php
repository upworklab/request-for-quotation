<?php

namespace App\Http\Requests\ApiServer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use DB;
use App\Models\ApiServer;

class UpdateApiServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('api_server'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:255',
            ],
            'base_uri' => [
                'required',
                'url',
                'max:255',
            ],
            'username' => [
                'required',
                'max:255',
            ],
            'password' => [
                'required',
                'max:255',
            ],
        ];
    }

    public function getValidatedApiServer()
    {
        $validated = parent::validated();
        $validated['avatar_path'] = $this->hasFile('avatar') ? $this->file('avatar')->store('public/avatar') : '';

        return array_filter($validated);
    }

    public function fulfill(ApiServer $user)
    {
        DB::beginTransaction();

        try {
            // Save ApiServer
            $user->update($this->getValidatedApiServer());

            if ($this->filled('role')) {
                // Sync ApiServer - Roles
                $selected_role = Role::findOrFail($this->input('role'));

                $user->roles()->sync($selected_role->id);

                if ($selected_role->name == 'administrator') {
                    // Create related role
                    $related_role = $user->administrator ?: new Administrator;
                    // $related_role->full_name = $this->input('name');

                    // Sync ApiServer - related role
                    $user->administrator()->save($related_role);

                } else if ($selected_role->name == 'client') {
                    // Create related role
                    $related_role = $user->client ?: new Client;
                    $related_role->fill(array_filter([
                        'full_name' => $this->input('full_name'),
                        'alias' => $this->input('domain'),
                        'domain' => $this->input('alias'),
                    ]));

                    // Sync ApiServer - related role
                    $user->client()->save($related_role);

                } else if ($selected_role->name == 'vendor') {
                    // Create related role
                    $related_role = $user->vendor ?: new Vendor;
                    $related_role->fill(array_filter([
                        'full_name' => $this->input('full_name'),
                        'address' => $this->input('address'),
                    ]));

                    // Sync ApiServer - related role
                    $user->vendor()->save($related_role);
                }
            }

            DB::commit();

            return $user;

        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            abort(500);
            // return false;
        }
    }
}

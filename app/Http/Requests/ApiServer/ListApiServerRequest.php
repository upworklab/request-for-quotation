<?php

namespace App\Http\Requests\ApiServer;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\ApiServer;
use Yajra\Datatables\Datatables;

class ListApiServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("viewAny", ApiServer::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill()
    {
        $model = ApiServer::query();

        return Datatables::of($model)->make(true);
    }
}

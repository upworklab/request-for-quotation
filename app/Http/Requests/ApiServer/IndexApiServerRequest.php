<?php

namespace App\Http\Requests\ApiServer;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\ApiServer;

class IndexApiServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("viewAny", ApiServer::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill()
    {
        return view('api-server.administrator.index');
    }
}

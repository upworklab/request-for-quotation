<?php

namespace App\Http\Requests\ApiServer;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\ApiServer;

class CreateApiServerRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user()->can("create", ApiServer::class);
    }

    public function rules()
    {
        return [
            //
        ];
    }
}

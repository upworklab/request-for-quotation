<?php

namespace App\Http\Requests\Quotation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use DB;
use Carbon\Carbon;
use App\Models\Quotation;
use App\Models\QuotationItem;

class FinalizeQuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("update", $this->route("quotation"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor' => [
                Rule::requiredIf($this->user()->hasRole('administrator')),
                'exists:App\Models\Vendor,remote_id',
            ],
            'valid_until' => [
                'required',
                'date_format:d/m/Y',
            ],
            'remark' => [
                'nullable',
                'string',
            ],
            'product.*' => [
                'required',
                'numeric',
            ],
            'quantity.*' => [
                'nullable',
                'string',
            ],
            'quoted_quantity.*' => [
                'required',
                'numeric',
            ],
            'uom.*' => [
                'required',
                'numeric',
            ],
            'quoted_price.*' => [
                'required',
                'numeric',
            ],
        ];
    }

    public function validatedData()
    {
        $validated = parent::validated();
        $model_attributes = \Illuminate\Support\Arr::except($validated, ['vendor', 'valid_until']);
        return array_merge($model_attributes, [
            'remote_partner_id' => $this->user()->hasRole('administrator') ? $validated['vendor'] : $this->user()->vendor->remote_id,
            'remote_valid_until' => $validated['valid_until'],
            // 'purchase_code' => rand(10000,99999),
            // 'total_value' => 0,
        ]);
    }

    public function fulfill(Quotation $quotation)
    {
        DB::beginTransaction();

        try {
            // Update model
            $validated_data = $this->validatedData();
            $quotation->update($validated_data);
            $quotation->items()->delete();

            // $total_value = $quotation->total_value;
            foreach ($validated_data['product'] as $index => $item) {
                if (($validated_data['quoted_quantity'][$index] != "")
                    AND ($validated_data['quoted_price'][$index] != "")) {
                        $item = new QuotationItem([
                            'product_id' => $validated_data['product'][$index],
                            'uom_id' => $validated_data['uom'][$index],
                            'quantity' => $validated_data['quoted_quantity'][$index],
                            'quoted_price' => $validated_data['quoted_price'][$index],
                        ]);
                        $quotation->items()->save($item);
                        // $total_value = $total_value + $validated_data['quoted_price'][$index];
                }
            }

            // $quotation->total_value = $total_value;
            $quotation->save();

            DB::commit();

            return $this->sendToServer($quotation->id);

        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            abort(500);
            // return false;
        }
    }

    private function sendToServer($quotationId)
    {
        $quotation = Quotation::find($quotationId);

        $quotationItems = [];
        foreach ($quotation->items as $item) {
            if ($item->quantity > 0) {
                $quotationItems[] = [
                    'product' => $item->product->remote_id,
                    'quantity_requested' => $item->quantity,
                    'quantity_quoted' => $item->quantity,
                    'uom' => $item->uom->remote_id,
                    'price_quoted' => $item->quoted_price,
                ];
            }
        }

        // Call Artisan command (Submit)
        $postQuotation = \Artisan::call('amtiss:post-quotation', [
            '--vendorId' => $this->user()->vendor->remote_id,
            '--locationId' => $quotation->location->remote_id,
            '--orderDate' => now()->format('Y-m-d H:i:s'),
            '--validUntil' => Carbon::createFromFormat('d/m/Y', $quotation->remote_valid_until)->format('Y-m-d'),
            '--purchaseRequestId' => $quotation->remote_requisition_id,
            '--items' => json_encode($quotationItems),
            '--remark' => $quotation->remark,
        ]);

        $updatePurchaseOrder = \Artisan::call('amtiss:get-purchase-list');
        $updatePurchaseOrderItems = \Artisan::call('amtiss:get-purchase-items');

        /*
        $postQuotation = \Artisan::call('amtiss:post-quotation', [
            '--vendorId' => 1234142123,
            '--locationId' => 3425365345346,
            '--orderDate' => now()->format('Y-m-d H:i:s'),
            '--validUntil' => 34324234324,
            '--purchaseRequestId' => 2314234234234324,
            '--items' => '{}',
            '--remark' => '234324234',
        ]);
        */

        if ( ! $postQuotation) {
            $quotation->items()->delete();
            $quotation->delete();

            return true;
        }

        return false;

    }
}

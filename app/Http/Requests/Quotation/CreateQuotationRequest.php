<?php

namespace App\Http\Requests\Quotation;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Quotation;
use App\Models\PurchaseRequest;

class CreateQuotationRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user()->can("create", Quotation::class);
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill(PurchaseRequest $purchase_request = null)
    {
        if ($this->user()->administrator) {
            return view('quotation.administrator.create')
                ->with('purchase_request', $purchase_request)
                ->with('vendors', \App\Models\Vendor::active()->get());

        } elseif ($this->user()->vendor) {
            return view('quotation.vendor.create')
                ->with('purchase_request', $purchase_request);
        }

    }
}

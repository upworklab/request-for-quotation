<?php

namespace App\Http\Requests\Quotation;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Quotation;
use App\Models\PurchaseRequest;

class IndexQuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("viewAny", Quotation::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill(PurchaseRequest $purchase_request = null)
    {
        if ($this->user()->administrator) {
            if ($purchase_request) {
                return view('quotation.administrator.purchase-request')
                    ->with('purchase_request', $purchase_request)
                    ->with('quotations', $purchase_request->quotations);
            } else {
                return view('quotation.administrator.index');
            }

        } elseif ($this->user()->vendor) {
            return view('quotation.vendor.index');
        }
    }
}

<?php

namespace App\Http\Requests\Quotation;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Quotation;
use App\Models\PurchaseRequest;
use Yajra\Datatables\Datatables;

class ListQuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("viewAny", Quotation::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill(PurchaseRequest $purchase_request = null)
    {
        if ($this->user()->administrator) {
            if ($purchase_request) {
                $model = $purchase_request->quotations;
                $model->load('vendor');
            } else {
                $model = Quotation::all();
                $model->load('purchaseRequest.client.user', 'vendor.user');
            }

        } elseif ($this->user()->vendor) {
            $model = $this->user()->vendor->quotations->load(['client', 'location']);
            // $model = PurchaseRequest::active()->with(['client', 'location'])->hasRequisition()->select('purchase_requests.*');
            // $model->load('purchaseRequest.client');
        }

        return Datatables::of($model)->make(true);
    }
}

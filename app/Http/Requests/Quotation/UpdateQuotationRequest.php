<?php

namespace App\Http\Requests\Quotation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use DB;
use App\Models\Quotation;
use App\Models\QuotationItem;

class UpdateQuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("update", $this->route("quotation"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor' => [
                Rule::requiredIf($this->user()->hasRole('administrator')),
                'exists:App\Models\Vendor,remote_id',
            ],
            'valid_until' => [
                'required',
                'date_format:d/m/Y',
            ],
            'remark' => [
                'nullable',
                'string',
            ],
            'product.*' => [
                'required',
                'numeric',
            ],
            'quantity.*' => [
                'nullable',
                'string',
            ],
            'quoted_quantity.*' => [
                'required',
                'numeric',
            ],
            'uom.*' => [
                'required',
                'numeric',
            ],
            'quoted_price.*' => [
                'required',
                'numeric',
            ],
        ];
    }

    public function validatedData()
    {
        $validated = parent::validated();
        $model_attributes = \Illuminate\Support\Arr::except($validated, ['vendor', 'valid_until']);
        return array_merge($model_attributes, [
            'remote_partner_id' => $this->user()->hasRole('administrator') ? $validated['vendor'] : $this->user()->vendor->remote_id,
            'remote_valid_until' => $validated['valid_until'],
            // 'purchase_code' => rand(10000,99999),
            // 'total_value' => 0,
        ]);
    }

    public function fulfill(Quotation $quotation)
    {
        DB::beginTransaction();

        try {
            // Update model
            $validated_data = $this->validatedData();
            $quotation->update($validated_data);
            $quotation->items()->delete();

            // $total_value = $quotation->total_value;
            foreach ($validated_data['product'] as $index => $item) {
                if (($validated_data['quoted_quantity'][$index] != "")
                    AND ($validated_data['quoted_price'][$index] != "")) {
                        $item = new QuotationItem([
                            'product_id' => $validated_data['product'][$index],
                            'uom_id' => $validated_data['uom'][$index],
                            'quantity' => $validated_data['quoted_quantity'][$index],
                            'quoted_price' => $validated_data['quoted_price'][$index],
                        ]);
                        $quotation->items()->save($item);
                        // $total_value = $total_value + $validated_data['quoted_price'][$index];
                }
            }

            // $quotation->total_value = $total_value;
            $quotation->save();

            DB::commit();

            return $quotation;

        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            abort(500);
            // return false;
        }
    }
}

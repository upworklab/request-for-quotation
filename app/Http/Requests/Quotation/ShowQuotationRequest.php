<?php

namespace App\Http\Requests\Quotation;

use Illuminate\Foundation\Http\FormRequest;
// use App\Models\PurchaseRequest;
use App\Models\Quotation;

class ShowQuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("view", $this->route("quotation"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function fulfill(Quotation $quotation)
    {
        if ($this->user()->administrator) {
            return view('quotation.administrator.show')
                ->with('quotation', $quotation)
                ->with('purchase_request', $quotation->purchaseRequest)
                ->with('vendors', \App\Models\Vendor::active()->get());

        } elseif ($this->user()->vendor) {
            return view('quotation.vendor.show')
                ->with('quotation', $quotation);
        }

    }
}

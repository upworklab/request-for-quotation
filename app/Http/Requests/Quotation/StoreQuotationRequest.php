<?php

namespace App\Http\Requests\Quotation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use DB;
use App\Models\Quotation;
use App\Models\QuotationItem;
use App\Models\PurchaseRequest;

class StoreQuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("create", Quotation::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor' => [
                Rule::requiredIf($this->user()->hasRole('administrator')),
                'exists:App\Models\Vendor,remote_id',
            ],
            'valid_until' => [
                'required',
                'date_format:d/m/Y',
            ],
            'remark' => [
                'nullable',
                'string',
            ],
            'product.*' => [
                'required',
                'numeric',
            ],
            'quantity.*' => [
                'nullable',
                'string',
            ],
            'quoted_quantity.*' => [
                'required',
                'numeric',
            ],
            'uom.*' => [
                'required',
                'numeric',
            ],
            'quoted_price.*' => [
                'required',
                'numeric',
            ],
        ];
    }

    public function validatedData($purchase_request)
    {
        $validated = parent::validated();
        $model_attributes = \Illuminate\Support\Arr::except($validated, ['vendor', 'valid_until']);
        return array_merge($model_attributes, [
            'remote_partner_id' => $this->user()->hasRole('administrator') ? $validated['vendor'] : $this->user()->vendor->remote_id,
            'client_id' => $purchase_request->client_id,
            'location_id' => $purchase_request->location_id,
            'expected_date' => $validated['valid_until'],
            'remote_valid_until' => $validated['valid_until'],
            'remote_requisition_id' => $purchase_request->remote_requisition_id,
            'purchase_code' => rand(10000,99999),
            // 'purchase_request_id' => $purchase_request->id,
            // 'total_value' => 0,
        ]);
    }

    public function fulfill(PurchaseRequest $purchase_request)
    {
        DB::beginTransaction();

        try {
            // Store to DB
            // Create model
            $validated_data = $this->validatedData($purchase_request);
            $model = Quotation::create($validated_data);

            foreach ($validated_data['product'] as $index => $item) {
                if (($validated_data['quoted_quantity'][$index] != "")
                    AND ($validated_data['quoted_price'][$index] != "")) {
                        $item = new QuotationItem([
                            'product_id' => $validated_data['product'][$index],
                            'uom_id' => $validated_data['uom'][$index],
                            'quantity' => $validated_data['quoted_quantity'][$index],
                            'quoted_price' => $validated_data['quoted_price'][$index],
                        ]);
                        $model->items()->save($item);
                }
            }

            $model->save();

            DB::commit();

            return $model;

        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            dd($e);
            abort(500);
            // return false;
        }
    }
}

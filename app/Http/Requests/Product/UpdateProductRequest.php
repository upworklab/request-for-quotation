<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use DB;
use App\Models\Product;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasRole('administrator');
        // return $this->user()->can("update", Product::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:255',
            ],
            'code' => [
                'required',
                'max:255',
            ],
            'description' => [
                'required',
                'max:255',
            ],
        ];
    }

    public function fulfill(Product $product)
    {
        DB::beginTransaction();

        try {
            // Save Product
            $product->name = $this->input('name');
            $product->product_code = $this->input('code');
            $product->description = $this->input('description');
            $product->save();

            DB::commit();

            return $product;

        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            abort(500);
            // return false;
        }
    }
}

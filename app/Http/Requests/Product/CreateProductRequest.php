<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Product;

class CreateProductRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user()->hasRole('administrator');
        // return $this->user()->can("create", Product::class);
    }

    public function rules()
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use DB;
use App\Models\Role;
use App\Models\User;
use App\Models\Client;
use App\Models\Vendor;
use App\Models\Administrator;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:255',
            ],
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore($this->route('user')),
            ],
            'password' => [
                'nullable',
                'confirmed',
            ],
            'full_name' => [
                'nullable',
                'string',
            ],
            'alias' => [
                'nullable',
                'string',
            ],
            'domain' => [
                'nullable',
                'string',
            ],
            'address' => [
                'nullable',
                'string',
            ],
            'avatar' => [
                'nullable',
                'image',
            ],
        ];
    }

    public function getValidatedUser()
    {
        $validated = parent::validated();
        $validated['avatar_path'] = $this->hasFile('avatar') ? $this->file('avatar')->store('public/avatar') : '';

        return array_filter($validated);
    }

    public function fulfill(User $user = null)
    {
        DB::beginTransaction();

        try {
            $user = $this->user();

            // Save User
            $user->update($this->getValidatedUser());

            if ($user->hasRole('administrator')) {
                // Create related role
                $related_role = $user->administrator ?: new Administrator;
                // $related_role->full_name = $this->input('name');

                // Sync User - related role
                $user->administrator()->save($related_role);

            } else if ($user->hasRole('client')) {
                // Create related role
                $related_role = $user->client ?: new Client;
                $related_role->fill(array_filter([
                    'full_name' => $this->input('full_name'),
                    'alias' => $this->input('alias'),
                    'domain' => $this->input('domain'),
                ]));

                // Sync User - related role
                $user->client()->save($related_role);

            } else if ($user->hasRole('vendor')) {
                // Create related role
                $related_role = $user->vendor ?: new Vendor;
                $related_role->fill(array_filter([
                    'full_name' => $this->input('full_name'),
                    'address' => $this->input('address'),
                ]));

                // Sync User - related role
                $user->vendor()->save($related_role);
            }

            DB::commit();

            return $user;

        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            abort(500);
            // return false;
        }
    }
}

<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class CreateUserRequest extends FormRequest
{
    public function authorize()
    {
        // return $this->user()->hasRole('administrator');
        return $this->user()->can("create", User::class);
    }

    public function rules()
    {
        return [
            //
        ];
    }
}

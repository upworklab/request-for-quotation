<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use DB;
use App\Models\Role;
use App\Models\User;
use App\Models\Client;
use App\Models\Vendor;
use App\Models\Administrator;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("create", User::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:255',
            ],
            'email' => [
                'required',
                'email',
                'unique:App\Models\User,email',
            ],
            'password' => [
                'required',
                'confirmed',
            ],
            'role' => [
                'required',
                'exists:App\Models\Role,id',
            ],
            'full_name' => [
                'nullable',
                'string',
            ],
            'alias' => [
                'nullable',
                'string',
            ],
            'domain' => [
                'nullable',
                'string',
            ],
            'address' => [
                'nullable',
                'string',
            ],
            'avatar' => [
                'nullable',
                'image',
            ],
            'vendor_id' => [
                'nullable',
                'unique:App\Models\Vendor,remote_id',
            ],
        ];
    }

    public function getValidatedUser()
    {
        $validated = parent::validated();
        $validated['avatar_path'] = $this->hasFile('avatar') ? $this->file('avatar')->store('public/avatar') : '';

        return array_filter($validated);
    }

    public function fulfill()
    {
        DB::beginTransaction();

        try {
            // Create user
            $user = User::create($this->getValidatedUser());

            // Sync User - Roles
            $selected_role = Role::findOrFail($this->input('role'));
            $user->roles()->sync($selected_role->id);

            if ($user->hasRole('administrator')) {
                // Create related role
                $related_role = new Administrator;
                $user->administrator()->save($related_role);

            } else if ($user->hasRole('client')) {
                // Create related role
                $related_role = new Client;
                $related_role->fill(array_filter([
                    'full_name' => $this->input('full_name'),
                    'alias' => $this->input('domain'),
                    'domain' => $this->input('alias'),
                ]));

                // Sync User - related role
                $user->client()->save($related_role);

            } else if ($selected_role->name == 'vendor') {
                // Create related role
                $related_role = new Vendor;
                $related_role->fill(array_filter([
                    'full_name' => $this->input('full_name'),
                    'address' => $this->input('address'),
                    'remote_id' => $this->input('vendor_id'),
                ]));

                // Sync User - related role
                $user->vendor()->save($related_role);
            }

            DB::commit();

            return $user;

        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            dd($e);
            abort(500);
            // return false;
        }
    }
}

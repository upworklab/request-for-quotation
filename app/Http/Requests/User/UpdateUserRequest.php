<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use DB;
use App\Models\Role;
use App\Models\User;
use App\Models\Client;
use App\Models\Vendor;
use App\Models\Administrator;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('user'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->route('user')->vendor?->remote_id);
        return [
            'name' => [
                'required',
                'max:255',
            ],
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore($this->route('user')),
            ],
            'password' => [
                'nullable',
                'confirmed',
            ],
            'role' => [
                'nullable',
                'exists:App\Models\Role,id',
            ],
            'full_name' => [
                'nullable',
                'string',
            ],
            'alias' => [
                'nullable',
                'string',
            ],
            'domain' => [
                'nullable',
                'string',
            ],
            'address' => [
                'nullable',
                'string',
            ],
            'avatar' => [
                'nullable',
                'image',
            ],
            'vendor_id' => [
                'nullable',
                Rule::unique('vendors', 'remote_id')->ignore($this->route('user')->vendor?->id),
                // 'unique:App\Models\Vendor,remote_id',
            ],
        ];
    }

    public function getValidatedUser()
    {
        $validated = parent::validated();
        $validated['avatar_path'] = $this->hasFile('avatar') ? $this->file('avatar')->store('public/avatar') : '';

        return array_filter($validated);
    }

    public function fulfill(User $user)
    {
        DB::beginTransaction();

        try {
            // Save User
            $user->update($this->getValidatedUser());

            if ($this->filled('role')) {
                // Sync User - Roles
                $selected_role = Role::findOrFail($this->input('role'));

                $user->roles()->sync($selected_role->id);

                if ($selected_role->name == 'administrator') {
                    // Create related role
                    $related_role = $user->administrator ?: new Administrator;
                    // $related_role->full_name = $this->input('name');

                    // Sync User - related role
                    $user->administrator()->save($related_role);

                } else if ($selected_role->name == 'client') {
                    // Create related role
                    $related_role = $user->client ?: new Client;
                    $related_role->fill(array_filter([
                        'full_name' => $this->input('full_name'),
                        'alias' => $this->input('domain'),
                        'domain' => $this->input('alias'),
                    ]));

                    // Sync User - related role
                    $user->client()->save($related_role);

                } else if ($selected_role->name == 'vendor') {
                    // Create related role
                    $related_role = $user->vendor ?: new Vendor;
                    $related_role->fill(array_filter([
                        'full_name' => $this->input('full_name'),
                        'address' => $this->input('address'),
                        'remote_id' => $this->input('vendor_id'),
                    ]));

                    // Sync User - related role
                    $user->vendor()->save($related_role);

                }
            }

            DB::commit();

            return $user;

        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            abort(500);
            // return false;
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Models\Vendor;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
// use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_legal_name' => ['required', 'string', 'max:255'],
            'year_of_establishment' => ['required', 'numeric'],
            'contact_name' => ['required', 'string', 'max:255'],
            'phone' => ['nullable', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'captcha' => ['required', 'captcha'],
        ]);

        $user = User::create([
            'name' => $request->contact_name,
            'phone' => $request->phone,
            'email' => $request->email,
            // 'password' => Hash::make($request->password),
            'password' => $request->password,
        ]);

        try {
            // Sync vendor role to user
            $vendor_role = Role::firstWhere('name', 'vendor');
            $user->roles()->sync($vendor_role->id);

            $related_role = new Vendor;
            $related_role->fill(array_filter([
                'full_name' => $request->company_legal_name,
                'address' => $request->address,
                'year_established' => $request->year_of_establishment,
            ]));

            $user->vendor()->save($related_role);

            event(new Registered($user));

            Auth::login($user);

            return redirect(RouteServiceProvider::HOME);

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect(RouteServiceProvider::HOME);
        }
    }
}

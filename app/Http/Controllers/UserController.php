<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\User\ListUserRequest;
use App\Http\Requests\User\ShowUserRequest;
use App\Http\Requests\User\IndexUserRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\UpdateProfileRequest;

class UserController extends Controller
{
    public function index(IndexUserRequest $request)
    {
        return $request->fulfill();
    }

    public function getDatatables(ListUserRequest $request)
    {
        return $request->fulfill();
    }

    public function create(CreateUserRequest $request)
    {
        return view('user.administrator.create');
    }

    public function store(StoreUserRequest $request)
    {
        $user = $request->fulfill();

        return redirect()->route('user.index')->with('success', __('User created successfully!'));
    }

    public function show(ShowUserRequest $request, User $user)
    {
        return $request->fulfill($user);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $request->fulfill($user);

        return redirect()->route('user.index')->with('success', __('User updated successfully!'));
    }

    public function showProfile()
    {
        $user = auth()->user();
        // $user->load('student');
        // $user->birth_date = $user->student?->birth_date;
        // $user->gender = $user->student?->gender;

        return view('user.profile')
            ->with('user', $user);
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $user = $request->fulfill();

        return redirect()->route('user.profile')->with('success', __('Profile updated successfully!'));
    }
}

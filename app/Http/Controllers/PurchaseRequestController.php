<?php

namespace App\Http\Controllers;

use App\Models\PurchaseRequest;
use App\Http\Requests\PurchaseRequest\ListPurchaseRequestRequest;
use App\Http\Requests\PurchaseRequest\ShowPurchaseRequestRequest;
use App\Http\Requests\PurchaseRequest\IndexPurchaseRequestRequest;
use App\Http\Requests\PurchaseRequest\StorePurchaseRequestRequest;
use App\Http\Requests\PurchaseRequest\CreatePurchaseRequestRequest;
use App\Http\Requests\PurchaseRequest\UpdatePurchaseRequestRequest;
use App\Http\Requests\PurchaseRequest\FinalizePurchaseRequestRequest;

class PurchaseRequestController extends Controller
{
    public function index(IndexPurchaseRequestRequest $request)
    {
        return $request->fulfill();
    }

    public function getDatatables(ListPurchaseRequestRequest $request)
    {
        return $request->fulfill();
    }

    public function create(CreatePurchaseRequestRequest $request)
    {
        return $request->fulfill();
    }

    public function store(StorePurchaseRequestRequest $request)
    {
        $request->fulfill();

        return redirect()->route('purchase-request.index');
    }

    public function show(ShowPurchaseRequestRequest $request, PurchaseRequest $purchaseRequest)
    {
        return $request->fulfill($purchaseRequest);
    }

    public function update(UpdatePurchaseRequestRequest $request, PurchaseRequest $purchaseRequest)
    {
        $request->fulfill($purchaseRequest);

        return redirect()->route('purchase-request.index');
    }

    public function finalize(FinalizePurchaseRequestRequest $request, PurchaseRequest $purchaseRequest)
    {
        $request->fulfill($purchaseRequest);

        return redirect()->route('purchase-request.index');
    }
}

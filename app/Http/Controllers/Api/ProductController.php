<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $model = \App\Models\Product::all();
        return response()->json($model);
    }

    public function search(Request $request)
    {
        $model = \App\Models\Product::where('name', 'like', "%{$request->input('q')}%")
            ->paginate(10);

        return ProductResource::collection($model);
    }
}

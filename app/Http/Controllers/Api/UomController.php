<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\UomResource;

class UomController extends Controller
{
    public function index(Request $request)
    {
        $model = \App\Models\Uom::all();
        return response()->json($model);
    }

    public function search(Request $request)
    {
        $model = \App\Models\Uom::where('name', 'like', "%{$request->input('q')}%")
            ->paginate(10);

        return UomResource::collection($model);
    }
}

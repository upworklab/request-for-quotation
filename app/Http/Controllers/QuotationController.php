<?php

namespace App\Http\Controllers;

use App\Models\Quotation;
use App\Models\PurchaseRequest;
use App\Http\Requests\Quotation\ListQuotationRequest;
use App\Http\Requests\Quotation\ShowQuotationRequest;
use App\Http\Requests\Quotation\IndexQuotationRequest;
use App\Http\Requests\Quotation\StoreQuotationRequest;
use App\Http\Requests\Quotation\CreateQuotationRequest;
use App\Http\Requests\Quotation\UpdateQuotationRequest;
use App\Http\Requests\Quotation\FinalizeQuotationRequest;

class QuotationController extends Controller
{
    public function index(IndexQuotationRequest $request, PurchaseRequest $purchaseRequest = null)
    {
        return $request->fulfill($purchaseRequest);
    }

    public function getDatatables(ListQuotationRequest $request, PurchaseRequest $purchaseRequest = null)
    {
        return $request->fulfill($purchaseRequest);
    }

    public function create(CreateQuotationRequest $request, PurchaseRequest $purchaseRequest = null)
    {
        return $request->fulfill($purchaseRequest);
    }

    public function store(StoreQuotationRequest $request, PurchaseRequest $purchaseRequest = null)
    {
        $quotation = $request->fulfill($purchaseRequest);

        return redirect()->route('quotation.show', $quotation)->with('success', __('Quotation saved successfully!'));
    }

    public function show(ShowQuotationRequest $request, Quotation $quotation)
    {
        return $request->fulfill($quotation);
    }

    public function update(UpdateQuotationRequest $request, Quotation $quotation)
    {
        $request->fulfill($quotation);

        return redirect()->route('quotation.show', $quotation)->with('success', __('Quotation updated successfully!'));
    }

    public function finalize(FinalizeQuotationRequest $request, Quotation $quotation)
    {
        $result = $request->fulfill($quotation);

        return $result
            ? redirect()->route('quotation.index')->with('success', __('Quotation sent successfully!'))
            : redirect()->route('quotation.index')->with('warning', __('Send quotation failed!'));
    }
}

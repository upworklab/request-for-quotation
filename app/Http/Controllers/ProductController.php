<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Requests\Product\ListProductRequest;
use App\Http\Requests\Product\ShowProductRequest;
use App\Http\Requests\Product\IndexProductRequest;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;

class ProductController extends Controller
{
    public function index(IndexProductRequest $request)
    {
        return $request->fulfill();
    }

    public function getDatatables(ListProductRequest $request)
    {
        return $request->fulfill();
    }

    public function create(CreateProductRequest $request)
    {
        return view('product.administrator.create');
    }

    public function store(StoreProductRequest $request)
    {
        $product = $request->fulfill();

        return redirect()->route('product.index');
    }

    public function show(ShowProductRequest $request, Product $product)
    {
        return $request->fulfill($product);
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $product = $request->fulfill($product);

        return redirect()->route('product.index');
    }

}

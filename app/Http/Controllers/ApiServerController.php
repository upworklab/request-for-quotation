<?php

namespace App\Http\Controllers;

use App\Models\ApiServer;
use App\Http\Requests\ApiServer\ListApiServerRequest;
use App\Http\Requests\ApiServer\ShowApiServerRequest;
use App\Http\Requests\ApiServer\IndexApiServerRequest;
use App\Http\Requests\ApiServer\StoreApiServerRequest;
use App\Http\Requests\ApiServer\CreateApiServerRequest;
use App\Http\Requests\ApiServer\UpdateApiServerRequest;
use App\Http\Requests\ApiServer\DestroyApiServerRequest;

class ApiServerController extends Controller
{
    public function index(IndexApiServerRequest $request)
    {
        return $request->fulfill();
    }

    public function getDatatables(ListApiServerRequest $request)
    {
        return $request->fulfill();
    }

    public function create(CreateApiServerRequest $request)
    {
        return view('api-server.administrator.create');
    }

    public function store(StoreApiServerRequest $request)
    {
        $user = $request->fulfill();

        return redirect()->route('api-server.index');
    }

    public function show(ShowApiServerRequest $request, ApiServer $apiServer)
    {
        return $request->fulfill($apiServer);
    }

    public function update(UpdateApiServerRequest $request, ApiServer $apiServer)
    {
        $request->fulfill($apiServer);

        return redirect()->route('api-server.index');
    }

    public function destroy(DestroyApiServerRequest $request, ApiServer $apiServer)
    {
        $request->fulfill($apiServer);

        return redirect()->route('api-server.index');
    }
}

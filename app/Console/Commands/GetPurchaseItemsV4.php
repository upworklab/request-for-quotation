<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\Uom;
use App\Models\Role;
use App\Models\User;
use App\Models\Client;
use App\Models\Product;
use App\Models\Location;
use App\Models\PurchaseRequest;
use App\Models\PurchaseRequestItem;

class GetPurchaseItemsV4 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amtiss:get-purchase-items-v4 {--P|purchase-list=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Purchase Items from remote server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $purchase_list_id = $this->option('purchase-list');

        if ( ! $purchase_list_id) {
            return $this->error('Must provide purchase list id');
        }

        $purchase_request = PurchaseRequest::ofRemoteId($purchase_list_id)->first();
        $token = $this->doLogin();
        $purchase_items = $this->getPurchaseOrderItems($token, $purchase_list_id);

        // // Filter locally existing data
        // $remote_ids = Arr::pluck($purchase_items['data'], 'id');
        // $pending_ids = array_diff($remote_ids, PurchaseRequest::pluck('remote_id')->toArray());

        // Loop through available purchase requests
        foreach ($purchase_items['data'] as $purchase_item) {

            // Only continue if remote_id not exits locally
            // if (in_array($purchase_item['id'], $pending_ids)) {

                $this->line('Processing id: '.$purchase_item['id']);
                $product = $this->getProduct($purchase_item['product_id']);
                $uom = $this->getUom($purchase_item['product_uom']);
                $this->processPurchaseItem($product, $uom, $purchase_request, $purchase_item);
            // }
        }

        return  0;
    }

    private function doLogin()
    {
        try {
            $remote_url = 'https://v35.amtiss.com/api/user/get_token';
            $response = Http::get($remote_url, [
                'login' => 'tana',
                'password' => '1234',
            ]);

            if ($response->failed()) {
                $response->throw();
            }

            return $response->json('token');

        } catch (\Exception $e) {
            dd($e);
        }
    }

    private function getPurchaseOrderItems($token, $purchaseListId)
    {
        $response_fields = [
            'id',
            'order_id',
            'product_id',
            'code_ref',
            'name',
            'product_qty',
            'product_uom',
            'price_unit',
            'state',
        ];

        try {
            $remote_url = 'https://v35.amtiss.com/api/purchase.order.line/find';
            $response = Http::get($remote_url, [
                'token' => $token,
                'fields' => $this->printArrayAsString($response_fields),
                'domain' => "[('id','=','".$purchaseListId."')]",
            ]);

            return $response->json();

        } catch (\Exception $e) {
            dd($e);
        }
    }

    private function printArrayAsString($array)
    {
        return "['". implode("','",$array) ."']";
    }

    private function getProduct($remoteProduct)
    {
        // Check product
        if ($product = Product::firstWhere('name', $remoteProduct[1])) {
            return $product;
        }

        return Product::factory()->create([
            'remote_id' => $remoteProduct[0],
            'name' => $remoteProduct[1],
        ]);
    }

    private function getUom($remoteUom)
    {
        // Check location
        if ($uom = Uom::firstWhere('name', $remoteUom[1])) {
            return $uom;
        }

        return Uom::factory()->create([
            'remote_id' => $remoteUom[0],
            'name' => $remoteUom[1],
        ]);
    }

    private function processPurchaseItem($product, $uom, $purchaseRequest, $purchaseItem)
    {
        // $expected_date = $purchaseOrder['minimum_planned_date']
        //     ? Carbon::createFromFormat('Y-m-d', $purchaseOrder['minimum_planned_date'])->format('d/m/Y')
        //     : $created_at->copy()->addWeek()->format('d/m/Y');

        $purchase_item = PurchaseRequestItem::factory()
            ->create([
                'remote_id' => $purchaseItem['id'],
                'purchase_request_id' => $purchaseRequest->id,
                'product_name' => $product->name,
                'quantity' => $purchaseItem['product_qty'],
                'uom' => $uom->name,
                // 'status' => strtoupper($purchaseItem['state']),
            ]);
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\Role;
use App\Models\User;
use App\Models\Client;
use App\Models\Vendor;
use App\Models\Location;
use App\Models\Quotation;
use App\Models\PurchaseRequest;
use App\Console\Commands\Traits\ApiCallTrait;

class GetPurchaseList extends Command
{
    use ApiCallTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amtiss:get-purchase-list {--reload=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Purchase List from remote server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $token = $this->doLogin();
        $purchase_orders = $this->getPurchaseOrder($token);

        // // Filter locally existing data
        // $remote_ids = Arr::pluck($purchase_orders['data'], 'id');
        // $pending_ids = array_diff($remote_ids, PurchaseRequest::pluck('remote_id')->toArray());

        // Loop through available purchase requests
        foreach ($purchase_orders['data'] as $purchase_order) {

            // Only continue if remote_id not exits locally
            // if (in_array($purchase_order['id'], $pending_ids)) {

                $this->line("Processing purchase id: {$purchase_order['id']}");
                \Log::info("Processing purchase id: {$purchase_order['id']}");

                $client = $this->getClientUser($purchase_order['company_id']);
                $vendor = $this->getVendorUser($purchase_order['partner_id']);
                $location = $this->getLocation($purchase_order['location_id']);
                $this->processPurchaseOrder($client, $vendor, $location, $purchase_order);
            // }
        }

        return  0;
    }

    private function getPurchaseOrder($token)
    {
        if ($this->option('reload')) {
            $lastRemoteId = 0;
        } else {
            $lastRemoteId = PurchaseRequest::count() > 0 ? PurchaseRequest::latest('remote_id')->first()->remote_id : 0;
        }

        $response_fields = [
            'id',
            'name',
            'date_order',
            // 'date_planned',
            'minimum_planned_date',
            'notes',
            'location_id',
            'company_id',
            'partner_id',
            'state',
            'requisition_id',
        ];

        try {
            $remote_url = 'https://v35.amtiss.com/api/purchase.order/find';
            $response = \Http::timeout(60*3)
                ->get($remote_url, [
                    'token' => $token,
                    'fields' => json_encode($response_fields),
                    'domain' => "[('state','in',('draft','sent')),('id', '>', ".$lastRemoteId.")]",
            ]);

            return $response->json();

        } catch (\Exception $e) {
            dd($e);
        }
    }

    private function getClientUser($remoteClient)
    {
        // Check client user
        if ($client = Client::findRemoteId($remoteClient[0])) {
            return $client;
        }

        // Check user email
        if ($user = User::firstWhere('email', $this->generateEmailAddress($remoteClient[1])) AND $user->client) {
            return $user->client;
        }

        $user = User::factory()
            ->hasAttached(Role::firstWhere('name', 'client'))
            ->has(
                Client::factory()->state([
                    'remote_id' => $remoteClient[0],
                    'full_name' => $remoteClient[1],
                ])
            )
            ->create([
                'name' => $remoteClient[1],
                'email' => $this->generateEmailAddress($remoteClient[1]),
            ]);

        return $user->client;
    }

    private function getVendorUser($remoteVendor)
    {
        // Check vendor user
        if ($vendor = Vendor::findRemoteId($remoteVendor[0])) {
            return $vendor;
        }

        // Check user email
        if ($user = User::firstWhere('email', $this->generateEmailAddress($remoteVendor[1])) AND $user->vendor) {
            return $user->vendor;
        }

        $user = User::factory()
            ->hasAttached(Role::firstWhere('name', 'vendor'))
            ->has(
                Vendor::factory()->state([
                    'remote_id' => $remoteVendor[0],
                    'full_name' => $remoteVendor[1],
                ])
            )
            ->create([
                'name' => $remoteVendor[1],
                'email' => Str::replace('-', '_', Str::slug(strtolower($remoteVendor[1]))).'@example.test',
            ]);

        return $user->vendor;
    }

    private function getLocation($remoteLocation)
    {
        // Check location
        if ($location = Location::findRemoteId($remoteLocation[0])) {
            return $location;
        }

        return Location::factory()->create([
            'remote_id' => $remoteLocation[0],
            'name' => $remoteLocation[1],
        ]);
    }

    private function processPurchaseOrder($client, $vendor, $location, $purchaseOrder)
    {
        $created_at = Carbon::parse($purchaseOrder['date_order']);
        $expected_date = $purchaseOrder['minimum_planned_date']
            ? Carbon::createFromFormat('Y-m-d', $purchaseOrder['minimum_planned_date'])->format('d/m/Y')
            : $created_at->copy()->addWeek()->format('d/m/Y');

        // Process purchase request
        if ($purchase_request = PurchaseRequest::findRemoteId($purchaseOrder['id'])) {
            $purchase_request->update([
                'client_id' => $client->id,
                'purchase_code' => $purchaseOrder['name'],
                'location_id' => $location->id,
                'expected_date' => $expected_date,
                'remark' => $purchaseOrder['notes'] ? $purchaseOrder['notes'] : null,
                'status' => strtoupper($purchaseOrder['state']),
                'requisition' => $purchaseOrder['requisition_id'] ? json_encode($purchaseOrder['requisition_id']) : null,
                'remote_requisition_id' => $purchaseOrder['requisition_id'] ? $purchaseOrder['requisition_id'][0] : null,
                'remote_partner_id' => $vendor->remote_id,
                'remote_valid_until' => $expected_date,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            $this->line("Purchase request updated: {$purchase_request->id}");
            \Log::info("Purchase request updated: {$purchase_request->id}");

        } else {
            $purchase_request = PurchaseRequest::factory()->create([
                'remote_id' => $purchaseOrder['id'],
                'client_id' => $client->id,
                'purchase_code' => $purchaseOrder['name'],
                'location_id' => $location->id,
                'expected_date' => $expected_date,
                'remark' => $purchaseOrder['notes'] ? $purchaseOrder['notes'] : null,
                'status' => strtoupper($purchaseOrder['state']),
                'requisition' => $purchaseOrder['requisition_id'] ? json_encode($purchaseOrder['requisition_id']) : null,
                'remote_requisition_id' => $purchaseOrder['requisition_id'] ? $purchaseOrder['requisition_id'][0] : null,
                'remote_partner_id' => $vendor->remote_id,
                'remote_valid_until' => $expected_date,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            $this->line("Purchase request created: {$purchase_request->id}");
            \Log::info("Purchase request created: {$purchase_request->id}");

        }

        // Process quotation
        if ($quotation = Quotation::findRemoteId($purchaseOrder['id'])) {
            $quotation->update([
                'client_id' => $client->id,
                'purchase_code' => $purchaseOrder['name'],
                'location_id' => $location->id,
                'expected_date' => $expected_date,
                'remark' => $purchaseOrder['notes'] ? $purchaseOrder['notes'] : null,
                'status' => 'SENT',
                'requisition' => $purchaseOrder['requisition_id'] ? json_encode($purchaseOrder['requisition_id']) : null,
                'remote_requisition_id' => $purchaseOrder['requisition_id'] ? $purchaseOrder['requisition_id'][0] : null,
                'remote_partner_id' => $vendor->remote_id,
                'remote_valid_until' => $expected_date,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            $this->line("Quotation updated: {$purchase_request->id}");
            \Log::info("Quotation updated: {$purchase_request->id}");

        } else {
            $quotation = Quotation::factory()->create([
                'remote_id' => $purchaseOrder['id'],
                'client_id' => $client->id,
                'purchase_code' => $purchaseOrder['name'],
                'location_id' => $location->id,
                'expected_date' => $expected_date,
                'remark' => $purchaseOrder['notes'] ? $purchaseOrder['notes'] : null,
                'status' => 'SENT',
                'requisition' => $purchaseOrder['requisition_id'] ? json_encode($purchaseOrder['requisition_id']) : null,
                'remote_requisition_id' => $purchaseOrder['requisition_id'] ? $purchaseOrder['requisition_id'][0] : null,
                'remote_partner_id' => $vendor->remote_id,
                'remote_valid_until' => $expected_date,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            $this->line("Quotation created: {$purchase_request->id}");
            \Log::info("Quotation created: {$purchase_request->id}");
        }
    }

    private function generateEmailAddress($userName)
    {
        // Str::replace('-', '_', Str::slug(strtolower($remoteVendor[1]))).'@example.test';
        return Str::replace('-', '_', Str::slug(strtolower($userName))).'@example.test';
    }

    // Quotation is similar with purchase request
    // The difference: Quotation has local data
    private function processQuotation()
    {

    }
}

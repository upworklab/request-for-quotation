<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\Uom;
use App\Models\Role;
use App\Models\User;
use App\Models\Client;
use App\Models\Product;
use App\Models\Location;
use App\Models\Quotation;
use App\Models\PurchaseRequest;
use App\Models\QuotationItem;
use App\Models\PurchaseRequestItem;
use App\Console\Commands\Traits\ApiCallTrait;

class GetPurchaseItems extends Command
{
    use ApiCallTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amtiss:get-purchase-items {--reload=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Purchase Items from remote server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $token = $this->doLogin();
        $purchase_items = $this->getPurchaseOrderItems($token);

        // // Filter locally existing data
        // $remote_ids = Arr::pluck($purchase_items['data'], 'id');
        // $pending_ids = array_diff($remote_ids, PurchaseRequest::pluck('remote_id')->toArray());

        // Loop through available purchase requests
        foreach ($purchase_items['data'] as $purchase_item) {

            // Only continue if remote_id not exits locally
            // if (in_array($purchase_item['id'], $pending_ids)) {

                $this->line("Processing item id: {$purchase_item['id']}");
                \Log::info("Processing item id: {$purchase_item['id']}");
                $product = $this->getProduct($purchase_item);
                $uom = $this->getUom($purchase_item['product_uom']);
                $order = $this->getOrder($purchase_item['order_id']);
                $this->processPurchaseItem($product, $uom, $order, $purchase_item);
            // }
        }

        return  0;
    }

    private function getPurchaseOrderItems($token)
    {
        if ($this->option('reload')) {
            $lastRemoteId = 0;
        } else {
            $lastRemoteId = PurchaseRequestItem::count() > 0 ? PurchaseRequestItem::latest('remote_id')->first()->remote_id : 0;
        }

        $response_fields = [
            'id',
            'order_id',
            'product_id',
            'code_ref',
            'name',
            'product_qty',
            'product_uom',
            'price_unit',
            'description',
            'state',
        ];

        try {
            $remote_url = 'https://v35.amtiss.com/api/purchase.order.line/find';
            $response = \Http::timeout(60*3)
                ->get($remote_url, [
                    'token' => $token,
                    'fields' => json_encode($response_fields),
                    'domain' => "[('id','>',". $lastRemoteId .")]",
                    // 'domain' => "[('id','=','".$purchaseListId."')]",
            ]);

            return $response->json();

        } catch (\Exception $e) {
            dd($e);
        }
    }

    private function getProduct($purchaseItem)
    {
        // Check product
        $remoteProduct = $purchaseItem['product_id'];
        if ($product = Product::findRemoteId($remoteProduct[0])) {
            return $product;
        }

        return Product::factory()->create([
            'remote_id' => $remoteProduct[0],
            'name' => $remoteProduct[1],
            'description' => $purchaseItem['description'] ? $purchaseItem['description'] : null,
        ]);
    }

    private function getUom($remoteUom)
    {
        // Check uom
        if ($uom = Uom::findRemoteId($remoteUom[0])) {
            return $uom;
        }

        return Uom::factory()->create([
            'remote_id' => $remoteUom[0],
            'name' => $remoteUom[1],
        ]);
    }

    private function getOrder($remoteOrder)
    {
        // Check order
        if ($order = PurchaseRequest::findRemoteId($remoteOrder[0])) {
            return $order;
        }

        return false;
    }

    private function processPurchaseItem($product, $uom, $purchaseRequest, $purchaseItem)
    {
        // $expected_date = $purchaseOrder['minimum_planned_date']
        //     ? Carbon::createFromFormat('Y-m-d', $purchaseOrder['minimum_planned_date'])->format('d/m/Y')
        //     : $created_at->copy()->addWeek()->format('d/m/Y');

        if ($purchaseRequest) {

            // Process purchase request
            if ($purchase_item = PurchaseRequestItem::findRemoteId($purchaseItem['id'])) {
                $purchase_item->update([
                    'purchase_request_id' => $purchaseRequest->id,
                    'product_id' => $product->id,
                    'uom_id' => $uom->id,
                    'quantity' => $purchaseItem['product_qty'],
                    'quoted_price' => $purchaseItem['price_unit'],
                ]);

                $this->line("Purchase item updated: {$purchase_item->id}");
                \Log::info("Purchase item updated: {$purchase_item->id}");

            } else {
                $purchase_item = PurchaseRequestItem::factory()->create([
                    'remote_id' => $purchaseItem['id'],
                    'purchase_request_id' => $purchaseRequest->id,
                    'product_id' => $product->id,
                    'uom_id' => $uom->id,
                    'quantity' => $purchaseItem['product_qty'],
                    'quoted_price' => $purchaseItem['price_unit'],
                ]);

                $this->line("Purchase item created: {$purchase_item->id}");
                \Log::info("Purchase item created: {$purchase_item->id}");
            }

            // Process quotation
            // Only continue if we have the quotation in our db
            if ($quotation = Quotation::findRemoteId($purchaseItem['order_id'])) {
                if ($quotation_item = QuotationItem::findRemoteId($purchaseItem['id'])) {
                    $quotation_item->update([
                        'quotation_id' => $quotation->id,
                        'product_id' => $product->id,
                        'uom_id' => $uom->id,
                        'quantity' => $purchaseItem['product_qty'],
                        'quoted_price' => $purchaseItem['price_unit'],
                    ]);

                    $this->line("Quotation item updated: {$quotation_item->id}");
                    \Log::info("Quotation item updated: {$quotation_item->id}");

                } else {
                    $quotation_item = QuotationItem::factory()->create([
                        'remote_id' => $purchaseItem['id'],
                        'quotation_id' => $quotation->id,
                        'product_id' => $product->id,
                        'uom_id' => $uom->id,
                        'quantity' => $purchaseItem['product_qty'],
                        'quoted_price' => $purchaseItem['price_unit'],
                    ]);

                    $this->line("Quotation item created: {$quotation_item->id}");
                    \Log::info("Quotation item created: {$quotation_item->id}");
                }
            }
        }
    }
}

<?php

namespace App\Console\Commands\Traits;

// use Illuminate\Support\Facades\Http;

trait ApiCallTrait
{
    private function doLogin()
    {
        try {
            $remote_url = 'https://v35.amtiss.com/api/user/get_token';
            $response = \Http::get($remote_url, [
                'login' => 'tana',
                'password' => '1234',
            ]);

            if ($response->failed()) {
                $response->throw();
            }

            return $response->json('token');

        } catch (\Exception $e) {
            dd($e);
        }
    }

}

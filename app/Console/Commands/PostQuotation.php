<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Commands\Traits\ApiCallTrait;

class PostQuotation extends Command
{
    use ApiCallTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amtiss:post-quotation
                            {--vendorId= : Partner ID}
                            {--locationId= : Location ID}
                            {--orderDate= : Order date (Y-m-d H:i:s)}
                            {--validUntil= : Quotation validity (Y-m-d)}
                            {--purchaseRequestId= : Requisition ID}
                            {--items= : Quotation items (json)}
                            {--remark= : Quotation remark}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post RFQ to remote server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $token = $this->doLogin();
        $quotation_id = $this->sendQuotation($token);
        $quotation_items = $this->sendQuotationItems($token, $quotation_id);
        return ($quotation_id) ? 0 : 1;
    }

    private function sendQuotation($token)
    {
        $quotation_fields = [
            'partner_id' => $this->option('vendorId'),
            'location_id' => $this->option('locationId'),
            'date_order' => $this->option('orderDate'),
            'minimum_planned_date' => $this->option('validUntil'),
            'requisition_id' => $this->option('purchaseRequestId'),
        ];

        if ($this->option('remark')) {
            $quotation_fields['notes'] = $this->option('remark');
        }

        try {
            $remote_url = 'https://v35.amtiss.com/api/purchase.order/insert';
            $encoded_parameters = json_encode($quotation_fields);

            $response = \Http::get($remote_url, [
                'token' => $token,
                'create_vals' => $encoded_parameters,
            ]);

            $this->info("Sending Quotation: {$encoded_parameters}, url: {$response->effectiveUri()->__toString()}");
            \Log::info("Sending Quotation: {$encoded_parameters}, url: {$response->effectiveUri()->__toString()}");

            $this->info("Quotation sent: {$response->body()}");
            \Log::info("Quotation sent: {$response->body()}");

            return $response->json('data.0.id');

        } catch (\Exception $e) {
            dd($e);
        }
    }

    private function sendQuotationItems($token, $quotation_id)
    {
        try {
            $remote_url = 'https://v35.amtiss.com/api/purchase.order.line/insert';
            $quotation_items = json_decode($this->option('items'));

            foreach ($quotation_items as $quotation_item) {
                $item_parameters = [
                    'order_id' => (int) $quotation_id,
                    'product_id' => (int) $quotation_item->product,
                    'product_qty' => (int) $quotation_item->quantity_quoted,
                    'qty_bid' => (int) $quotation_item->quantity_requested,
                    'product_uom' => (int) $quotation_item->uom,
                    'price_unit' => (int) $quotation_item->price_quoted,
                ];

                $encoded_parameters = json_encode($item_parameters);

                $response = \Http::get($remote_url, [
                    'token' => $token,
                    'create_vals' => $encoded_parameters,
                ]);

                $this->info("Sending Quotation item: {$encoded_parameters}, url: {$response->effectiveUri()->__toString()}");
                \Log::info("Sending Quotation item: {$encoded_parameters}, url: {$response->effectiveUri()->__toString()}");

                $this->info("Quotation item sent: {$response->body()}");
                \Log::info("Quotation item sent: {$response->body()}");
            }

            return true;

        } catch (\Exception $e) {
            dd($e);
        }
    }


}

<?php

namespace App\Policies;

use App\Models\PurchaseRequest;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PurchaseRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('administrator') OR $user->hasRole('vendor');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PurchaseRequest  $purchaseRequest
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, PurchaseRequest $purchaseRequest)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasRole('administrator') OR $user->hasRole('client');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PurchaseRequest  $purchaseRequest
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, PurchaseRequest $purchaseRequest)
    {
        return $user->hasRole('administrator') OR $user->hasRole('client');
    }
}

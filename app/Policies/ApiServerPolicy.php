<?php

namespace App\Policies;

use App\Models\ApiServer;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApiServerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('administrator');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApiServer  $apiServer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, ApiServer $apiServer)
    {
        return $user->hasRole('administrator');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasRole('administrator');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApiServer  $apiServer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, ApiServer $apiServer)
    {
        return $user->hasRole('administrator');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApiServer  $apiServer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, ApiServer $apiServer)
    {
        return $user->hasRole('administrator');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApiServer  $apiServer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, ApiServer $apiServer)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApiServer  $apiServer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, ApiServer $apiServer)
    {
        //
    }
}
